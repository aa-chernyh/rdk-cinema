<?php
	session_start();
	$mysqli = new mysqli("localhost", "rdknikpe_rdk", "rdkrdk", "rdknikpe_rdk");
	$mysqli->query("SET NAMES 'utf8'");
	
	$result = $mysqli->query("SELECT `name`, `password` FROM `users`");
	$login = false;
	for($i=0; $row = $result->fetch_assoc(); $i++){
		if(isset($_SESSION["login"]) && isset($_SESSION["login"])){
			if($row['name'] == $_SESSION["login"] && $row['password'] == $_SESSION["pass"]){
				$login = true;
			}
		}
		else{
			if($row['name'] == $_POST['login'] && $row['password'] == md5($_POST['pass'])){
				$login = true;
				$_SESSION["login"] = $_POST['login'];
				$_SESSION["pass"] = md5($_POST['pass']);
			}
		}
	}
	if($login == false){
		die("<p>Wrong login or password!</p>");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Система управления контентом</title>
	<link href="img/favicon.ico" rel="icon" type="image/x-icon">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="css/cms.css">
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="js/tcal.js"></script>
	<script src="js/cms.js"></script>
</head>
<body>
	<div class="row message-wrap">
		<div class="col-xs-12 message"></div>
	</div>
	<div class="row content">
		<div class="col-xs-1"></div>
		<div class="col-xs-10">
			<form>
				<fieldset>
					<legend>Добавить новый фильм:</legend>
					<div class="col-xs-4 names">
						<p>Название:</p>
						<p>Вертикальная картинка (с расширением):</p>
						<p>Горизонтальная картинка (с расширением):</p>
						<p>Жанр:</p>
						<p>Начало показа (дата):</p>
						<p>Конец показа (дата):</p>
						<p>Описание:</p>
					</div>
					<div class="col-xs-8 inputs">
						<p><input type="text" name="name" id="addFilmName"></p>
						<p><input type="text" name="v_img" id="addFilmImgV"> Залить в директорию /img</p>
						<p><input type="text" name="h_img" id="addFilmImgH"> Залить в директорию /img/carousel</p>
						<p>
							<select name="zhanr" id="addFilmZhanr">
								<option value="Биографический">Биографический</option>
								<option value="Боевик">Боевик</option>
								<option value="Вестерн">Вестерн</option>
								<option value="Военный">Военный</option>
								<option value="Детектив">Детектив</option>
								<option value="Документальный">Документальный</option>
								<option value="Драма">Драма</option>
								<option value="Исторический">Исторический</option>
								<option value="Кинокомикс">Кинокомикс</option>
								<option value="Комедия">Комедия</option>
								<option value="Криминал">Криминал</option>
								<option value="Мелодрама">Мелодрама</option>
								<option value="Мистика">Мистика</option>
								<option value="Музыка">Музыка</option>
								<option value="Мультфильм">Мультфильм</option>
								<option value="Мюзикл">Мюзикл</option>
								<option value="Научный">Научный</option>
								<option value="Приключения">Приключения</option>
								<option value="Семейный">Семейный</option>
								<option value="Триллер">Триллер</option>
								<option value="Ужасы">Ужасы</option>
								<option value="Фантастика">Фантастика</option>
								<option value="Фэнтези">Фэнтези</option>
							</select>
						</p>
						<p><input type="text" name="nachalo" class="tcal" id="addFilmNachalo"></p>
						<p><input type="text" name="konec" class="tcal" id="addFilmKonec"></p>
						<textarea name="description" id="addFilmDescription"></textarea>
						<p><input type="button" value="Добавить" id="addFilm" onclick="film();"></p>
						<p><input type="reset" value="Сбросить" onclick="closeMessage()"></p>
					</div>
				</fieldset>
			</form>
			<form>
				<fieldset>
					<legend>Удалить фильм:</legend>
					<div class="col-xs-4 names">
						<p>Название:</p>
					</div>
					<div class="col-xs-8 inputs">
						<p>
							<select id="deleteFilmName">
								<?php
									$result = $mysqli->query("SELECT `id`, `name` FROM `current_films`");
									for($i=0; $row = $result->fetch_assoc(); $i++){
								?>
								<option value="<?=$row['id']?>"><?=$row['name']?></option>
								<?php } ?>
							</select>
						</p>
						<p><input type="button" value="Удалить" id="deleteFilm" onclick="delFilm()"></p>
						<!--<p><input type="button" value="Удалить старые фильмы" id="deleteOldFilms" onclick="delOldFilms()"></p>-->
					</div>
				</fieldset>
			</form>
			<form>
				<fieldset>
					<legend>Добавить новый сеанс:</legend>
					<div class="col-xs-4 names">
						<p>Фильм:</p>
						<div class="dateNames">
							<p>Дата:</p>
						</div>
						<p>Добавить: </p>
						<p>Время в формате "ЧЧ:ММ":</p>
						<p>Цена (только цифры):</p>
						<p>Постранство</p>
					</div>
					<div class="col-xs-8 inputs">
						<p>
							<select id="addSessionName">
								<?php
									$result = $mysqli->query("SELECT `id`, `name` FROM `current_films`");
									for($i=0; $row = $result->fetch_assoc(); $i++){
								?>
								<option value="<?=$row['id']?>"><?=$row['name']?></option>
								<?php } ?>
							</select>
						</p>
						<div class="Dates">
							<p><input type="text" name="data" class="tcal" id="addSessionDate1"></p>
						</div>
						<p><input type="button" value="Добавить дату" onclick="createNewDate();"></p>
						<p><input type="text" name="vremya_ch" style="width: 5%" id="addSessionVremyaCh"> <span class="bold">:</span> <input type="text" name="vremya_m" style="width: 5%" id="addSessionVremyaM"></p>
						<p><input type="text" name="cena" id="addSessionCena"></p>
						<p>
							<select name="zhanr" id="addSessionSpace">
								<option value="2D">2D</option>
								<option value="3D">3D</option>
							</select>
						</p>
						<p><input type="button" value="Добавить" id="addSession" onclick="addsession()"></p>
						<p><input type="reset" value="Сбросить" onclick="closeMessage()"></p>
					</div>
				</fieldset>
			</form>
			<form>
				<fieldset>
					<legend>Удалить сеанс:</legend>
					<div class="col-xs-4 names">
						<p>Фильм:</p>
						<p>Время:</p>
					</div>
					<div class="col-xs-8 inputs">
						<p>
							<select id="delSessionName">
								<?php
									$result = $mysqli->query("SELECT `id`, `name` FROM `current_films`");
									for($i=0; $row = $result->fetch_assoc(); $i++){
								?>
								<option value="<?=$row['id']?>"><?=$row['name']?></option>
								<?php } ?>
							</select>
						</p>
						<p>
							<select name="zhanr" id="delSessionVremya">
							</select>
						</p>
						<p><input type="button" value="Удалить" id="delSession" onclick="delsession()"></p>
						<!--<p><input type="button" value="Удалить прошедшие сеансы" id="delSessionAll" onclick="delOldsessions()"></p>-->
					</div>
				</fieldset>
			</form>
			<form>
				<fieldset>
					<legend>Редактировать фильм:</legend>
					<div class="col-xs-4 names">
						<p>Название:</p>
						<p>Вертикальная картинка (с расширением):</p>
						<p>Горизонтальная картинка (с расширением):</p>
						<p>Жанр:</p>
						<p>Начало показа (дата):</p>
						<p>Конец показа (дата):</p>
						<p>Описание:</p>
					</div>
					<div class="col-xs-8 inputs">
						<p>
							<select id="changeFilm">
								<?php
									$result = $mysqli->query("SELECT `id`, `name` FROM `current_films`");
									for($i=0; $row = $result->fetch_assoc(); $i++){
								?>
								<option value="<?=$row['id']?>"><?=$row['name']?></option>
								<?php } ?>
							</select>
						</p>
						<p><input type="text" name="v_img" id="changeFilmImgV"> Залить в директорию /img</p>
						<p><input type="text" name="h_img" id="changeFilmImgH"> Залить в директорию /img/carousel</p>
						<p>
							<select name="zhanr" id="changeFilmZhanr">
								<option value="Биографический">Биографический</option>
								<option value="Боевик">Боевик</option>
								<option value="Вестерн">Вестерн</option>
								<option value="Военный">Военный</option>
								<option value="Детектив">Детектив</option>
								<option value="Документальный">Документальный</option>
								<option value="Драма">Драма</option>
								<option value="Исторический">Исторический</option>
								<option value="Кинокомикс">Кинокомикс</option>
								<option value="Комедия">Комедия</option>
								<option value="Криминал">Криминал</option>
								<option value="Мелодрама">Мелодрама</option>
								<option value="Мистика">Мистика</option>
								<option value="Музыка">Музыка</option>
								<option value="Мультфильм">Мультфильм</option>
								<option value="Мюзикл">Мюзикл</option>
								<option value="Научный">Научный</option>
								<option value="Приключения">Приключения</option>
								<option value="Семейный">Семейный</option>
								<option value="Триллер">Триллер</option>
								<option value="Ужасы">Ужасы</option>
								<option value="Фантастика">Фантастика</option>
								<option value="Фэнтези">Фэнтези</option>
							</select>
						</p>
						<p><input type="text" name="nachalo" class="tcal" id="changeFilmNachalo"></p>
						<p><input type="text" name="konec" class="tcal" id="changeFilmKonec"></p>
						<textarea name="description" id="changeFilmDescription"></textarea>
						<p><input type="button" value="Изменить" id="addFilm" onclick="changefilm();"></p>
						<p><input type="reset" value="Сбросить" onclick="closeMessage()"></p>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="col-xs-1"></div>
	</div>
	<div class="row comments">
		<div class="col-md-1"></div>
		<div class="col-md-10 col-xs-12">
			<h1>Новые комментарии:</h1>
			<div class="commentTable">
				<table border="1">
					<tr>
						<th>Имя</th>
						<th>Комментарий</th>
						<th>Ответить</th>
						<th>Подтвердить</th>
					</tr>
					<?php
						$result = $mysqli->query("SELECT `id`, `name` , `comment` FROM `comments` WHERE `ok`= 0 ORDER BY `id`");
						for($i=0; $row = $result->fetch_assoc(); $i++){
					?>
					<tr id="tr<?=$row['id']?>">
						<td><?=$row['name']?></td>
						<td><?=$row['comment']?></td>
						<td><input type="text" name="otvet" class="otvet" id="otvet<?=$row['id']?>"></td>
						<td class="pointer">
							<i class="fa fa-check fa-2x" aria-hidden="true" class="pointer" style="color: green" onclick="comment_ok(<?=$row['id']?>, 1)"></i>
							<i class="fa fa-times fa-2x" style="color: red" class="pointer" aria-hidden="true" onclick="comment_ok(<?=$row['id']?>, 2)"></i>
						</td>
					</tr>
					<?php
						}
					?>
				</table>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</body>
</html>

<?php
	$mysqli->close();
?>