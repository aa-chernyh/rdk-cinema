<?php
$mysqli = new mysqli("localhost", "rdknikpe_rdk", "rdkrdk", "rdknikpe_rdk");
$mysqli->query("SET NAMES 'utf8'");
	setcookie("zhanr", $_POST['zhanr']);
	setcookie("space", $_POST['space']);
	setcookie("filmName", $_POST['filmName']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Расписание сеансов - Кинозал РДК</title>
	<link href="img/favicon.ico" rel="icon" type="image/x-icon">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/tcal.css" />
	<script type="text/javascript" src="js/tcal.js"></script>
	<script src="js/index.js"></script>
	<script src="js/raspisanie.js"></script>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>
</head>
<body>
	<div class="row top-row">
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</div>
		<div class="col-md-6 top-text col-xs-12"><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</div>
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></div>
	</div>
	<div class="row">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://кино-рдк.рф"><i class="fa fa-film" aria-hidden="true"></i> Кинозал РДК</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="http://кино-рдк.рф">Главная</a></li>
						<li class="active"><a href="http://кино-рдк.рф/расписание">Расписание сеансов</a></li>
						<li><a href="http://кино-рдк.рф#soon">Скоро в кино</a></li>
						<li><a href="http://кино-рдк.рф/цены">Цены билетов</a></li>
						<li><a href="http://кино-рдк.рф/план">План зала</a></li>
						<li><a href="http://кино-рдк.рф/правила">Правила</a></li>
						<li><a href="http://кино-рдк.рф/отзывы">Отзывы</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<div class="row raspisanie">
		<h1>Расписание сеансов</h1>
		<div class="col-md-8 col-sm-12 ras-wrap">
			<div class="table">
				<table>
					<tr>
						<th>Название фильма</th>
						<th>Жанр</th>
						<th>Время</th>
						<th>Дата</th>
						<th>Цена</th>
						<th>Пространство</th>
					</tr>
					<?php
					$query_1 = "SELECT CAST(`vremya` AS CHAR(5)), `data` , `cena` , `space`, `name`, `zhanr` FROM `sessions`, `current_films` WHERE `sessions`.`film_id` = `current_films`.`id` AND `sessions`.`data` >= CURDATE()";
					$query_2 = " ORDER BY `sessions`.`data`, `sessions`.`vremya`";
					if(isset($_POST['datafilter'])){
						if($_POST['datafilter'] != ""){
							$query_1 = $query_1 . " AND `sessions`.`data` = '" . $_POST['datafilter'] . "'";
						}

						if($_POST['cena_ot'] != ""){
							$query_1 = $query_1 . " AND `sessions`.`cena` >= '" . $_POST['cena_ot'] . "'";
						}

						if($_POST['cena_do'] != ""){
							$query_1 = $query_1 . " AND `sessions`.`cena` <= '" . $_POST['cena_do'] . "'";
						}

						if($_POST['zhanr'] != "Любой"){
							$query_1 = $query_1 . " AND `current_films`.`zhanr` = '" . $_POST['zhanr'] . "'";
						}

						if($_POST['space'] != "Любое"){
							$query_1 = $query_1 . " AND `sessions`.`space` = '" . $_POST['space'] . "'";
						}

						if($_POST['filmName'] != "Любой"){
							$query_1 = $query_1 . " AND `current_films`.`id` = '" . $_POST['filmName'] . "'";
						}
					}
					$finalQuery = $query_1 . $query_2;
					$result = $mysqli->query($finalQuery);
					for($i=0; $row = $result->fetch_assoc(); $i++){
						?>
						<tr class="deleteTR">
							<td><?=$row['name']?></td>
							<td><?=$row['zhanr']?></td>
							<td><?=$row["CAST(`vremya` AS CHAR(5))"]?></td>
							<td><?=DateTime::createFromFormat('Y-m-d', $row['data'])->format('d.m.Y');?></td>
							<td><?=$row['cena']?> рублей</td>
							<td><?=$row['space']?></td>
						</tr>
						<?php
					}
					?>
				</table>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 filter">
			<div class="filter-wrap">
				<form id="formFilter" method="POST" action="расписание">
					<table>
						<tr>
							<th>
								<p>Применить фильтр</p>
								<th>
								</tr>
								<tr>
									<td>
										<span class="bold">Дата </span> 
										<div class="floatRight">
											<input style="width: 110px" type="text" value="<?php
												if(isset($_POST['datafilter']))
												echo $_POST['datafilter'];
											?>" name="datafilter" class="tcal" id="datafilter">
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<span class="bold">Цена </span>
										<div class="floatRight">от: 
											<input type="text" value="<?php
											if(isset($_POST['cena_ot']))
												echo $_POST['cena_ot'];
											?>" name="cena_ot" id="cena_ot" class="miniInput">
											до: 
											<input type="text" value="<?php
												if(isset($_POST['cena_do']))
												echo $_POST['cena_do'];
											?>" name="cena_do" id="cena_do" class="miniInput"></div>
										</td>
									</tr>
									<tr>
										<td>
											<span class="bold">Жанр: </span>
											<div class="floatRight">
												<select name="zhanr" id="zhanr">
													<option value="Любой">Любой</option>
													<option value="Биографический">Биографический</option>
													<option value="Боевик">Боевик</option>
													<option value="Вестерн">Вестерн</option>
													<option value="Военный">Военный</option>
													<option value="Детектив">Детектив</option>
													<option value="Документальный">Документальный</option>
													<option value="Драма">Драма</option>
													<option value="Исторический">Исторический</option>
													<option value="Кинокомикс">Кинокомикс</option>
													<option value="Комедия">Комедия</option>
													<option value="Криминал">Криминал</option>
													<option value="Мелодрама">Мелодрама</option>
													<option value="Мистика">Мистика</option>
													<option value="Музыка">Музыка</option>
													<option value="Мультфильм">Мультфильм</option>
													<option value="Мюзикл">Мюзикл</option>
													<option value="Научный">Научный</option>
													<option value="Приключения">Приключения</option>
													<option value="Семейный">Семейный</option>
													<option value="Триллер">Триллер</option>
													<option value="Ужасы">Ужасы</option>
													<option value="Фантастика">Фантастика</option>
													<option value="Фэнтези">Фэнтези</option>
												</select>
											</div>
											<td>
											</tr>
											<tr>
												<td>
													<span class="bold">Пространство: </span>
													<div class="floatRight">
														<select name="space" id="space">
															<option value="Любое">Любое</option>
															<option value="2D">2D</option>
															<option value="3D">3D</option>
														</select>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<span class="bold">Фильм: </span>
													<div class="floatRight">
														<select id="filmName" name="filmName">
															<option value="Любой">Любой</option>
															<?php
															$result = $mysqli->query("SELECT `id`, `name` FROM `current_films`");
															for($i=0; $row = $result->fetch_assoc(); $i++){
																?>
																<option value="<?=$row['id']?>"><?=$row['name']?></option>
																<?php } ?>
															</select>
														</div>
													</td>
												</tr>
												<tr class="filterOn" style="background-color: #F0F0F0;">
													<td>
														<input type="button" value="Сбросить" id="filterReset" class="submit" onclick="filter_reset();">
														<input type="submit" value="Применить" id="filterOn" class="submit">
													</td>
												</tr>
											</table>
										</form>
									</div>
								</div>
							</div>
							<div class="row bottom-row">
								<div class="col-md-1"></div>
								<div class="col-md-10">
									<div class="col-md-4">
										<!-- VK Widget -->
										<div id="vk_groups"></div>
										<script type="text/javascript">
										VK.Widgets.Group("vk_groups", {mode: 0, width: "auto"}, 158350900);
										</script>
									</div>
									<div class="col-md-4 contacts col-sm-12">
										<p class="footer-H">Контакты</p>
										<ul>
											<li><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</li>
											<li><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</li>
											<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></li>
										</ul>
									</div>
									<div class="col-md-4 feedback col-sm-12">
										<form action="php/sendMail.php" method="POST" id="send">
					<p class="footer-H">Напишите нам</p>
					<ul>
						<li>
							<input type="text" name="name" placeholder="Имя" id="name" class="formInput">
							<span class="nameValid"></span>
						</li>
						<li>
							<input type="text" name="mail" placeholder="E-mail" id="mail" class="formInput">
							<span class="mailValid"></span>
						</li>
						<li>
							<textarea name="message" id="message" placeholder="Сообщение" class="formInput"></textarea>
							<span class="textValid"></span>
						</li>
					</ul>
					<input type="button" value="ОТПРАВИТЬ" class="submit" onclick="valid_sendMail()">
				</form>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</body>
</html>
<?php
	$mysqli->close();
?>