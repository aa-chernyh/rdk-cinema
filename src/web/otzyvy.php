<?php
	$mysqli = new mysqli("localhost", "rdknikpe_rdk", "rdkrdk", "rdknikpe_rdk");
	$mysqli->query("SET NAMES 'utf8'");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Отзывы посетителей - Кинозал РДК</title>
	<link href="img/favicon.ico" rel="icon" type="image/x-icon">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="css/index.css">
	<link rel="stylesheet" href="css/otzyvy.css">
	<script src="js/index.js"></script>
	<script src="js/otzyvy.js"></script>
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>
</head>
<body>
	<div class="row top-row">
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</div>
		<div class="col-md-6 top-text col-xs-12"><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</div>
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></div>
	</div>
	<div class="row">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://кино-рдк.рф"><i class="fa fa-film" aria-hidden="true"></i> Кинозал РДК</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="http://кино-рдк.рф">Главная</a></li>
						<li><a href="http://кино-рдк.рф/расписание">Расписание сеансов</a></li>
						<li><a href="http://кино-рдк.рф#soon">Скоро в кино</a></li>
						<li><a href="http://кино-рдк.рф/цены">Цены билетов</a></li>
						<li><a href="http://кино-рдк.рф/план">План зала</a></li>
						<li><a href="http://кино-рдк.рф/правила">Правила</a></li>
						<li class="active"><a href="http://kino-rdk.ru/отзывы">Отзывы</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<div class="row otzyvy">
		<div class="col-md-3"></div>
		<div class="col-md-6 col-xs-12">
			<h1>Отзывы посетителей:</h1>
			<div class="comments-wrap">
				<form method="POST" action="php/sendComment.php" id="sendComment">
					<div class="comment clearfix">
						<div class="black-line clearfix">
							<img src="img/favicon.ico">
						</div>
						<div class="wr clearfix">
							<div class="com-img">
								<img src="img/anonim.png">
							</div>
							<div class="com-text">
								<input type="text" name="nameComment" placeholder="Ваше имя" class="nameComment">
								<span class="nameCommentValid"></span>
								<textarea placeholder="Комментарий..." name="comment" class="commentText"></textarea>
								<span class="commentValid"></span>
								<input type="button" value="ОТПРАВИТЬ" class="submit" onclick="valid_sendComment()">
							</div>
						</div>
					</div>
				</form>
				<?php
					$result = $mysqli->query("SELECT `name` , `comment` , `otvet` FROM `comments` WHERE `ok`= 1 ORDER BY `id`");
					for($i=0; $row = $result->fetch_assoc(); $i++){
				?>
				<div class="comment clearfix">
					<div class="black-line clearfix">
						<img src="img/favicon.ico">
					</div>
					<div class="wrp clearfix">
						<div class="comment-img">
							<img src="img/anonim.png">
						</div>
						<div class="comment-text">
							<div class="bold"><?=$row['name']?></div>
							<div class="otzyv"><?=$row['comment']?></div>
						</div>
					</div>
					<?php
						if($row['otvet'] != ""){
					?>
					<div class="otvet">
							<div class="comment-img">
								<img src="img/1bNMCLngtAI-min.jpg">
							</div>
							<div class="comment-text">
								<div class="bold">Администрация кинозала</div>
								<div class="otzyv"><?=$row['otvet']?></div>
							</div>
						</div>
					<?php
						}
					?>
					</div>
				<?php
					}
				?>
			</div>
		</div>
		<div class="col-md-3"></div>	
	</div>
	<div class="row bottom-row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="col-md-4">
				<!-- VK Widget -->
				<div id="vk_groups"></div>
				<script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 0, width: "auto"}, 158350900);
				</script>
			</div>
			<div class="col-md-4 contacts col-sm-12">
				<p class="footer-H">Контакты</p>
				<ul>
					<li><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</li>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></li>
				</ul>
			</div>
			<div class="col-md-4 feedback col-sm-12">
				<form action="php/sendMail.php" method="POST" id="send">
					<p class="footer-H">Напишите нам</p>
					<ul>
						<li>
							<input type="text" name="name" placeholder="Имя" id="name" class="formInput">
							<span class="nameValid"></span>
						</li>
						<li>
							<input type="text" name="mail" placeholder="E-mail" id="mail" class="formInput">
							<span class="mailValid"></span>
						</li>
						<li>
							<textarea name="message" id="message" placeholder="Сообщение" class="formInput"></textarea>
							<span class="textValid"></span>
						</li>
					</ul>
					<input type="button" value="ОТПРАВИТЬ" class="submit" onclick="valid_sendMail()">
				</form>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</body>
</html>
<?php
	$mysqli->close();
?>