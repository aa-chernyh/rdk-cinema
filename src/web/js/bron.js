var CenaFilma = 0;
var AllCost = 0;
var rand = 0;

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function paintPlaces(){//Рисуем места
	var ii = 0;
	var jj = 0;
	for(i=1; i<=19; i++)
	{
		ii = i;
		if(i < 10)
			ii = 0+String(ii);
		$('.places-wrapper').append('<div class="row r'+ii+'"> <div class="col-md-2"> <div class="ryad"><p>ряд '+i+'</p></div></div>')
		$('.r'+ii).append('<div class="col-md-10 ryad-wrap ryad'+ii+'">');
		if(i==1)
			for(j=1; j<=16; j++){
				jj = j;
				if(j < 10)
					jj = 0+String(jj);
				$('.ryad'+ii).append('<span class="place" id="r'+ii+'m'+jj+'"><p>'+j+'</p></span>');
			}
		else if(i==2 || i==12)
			for(j=1; j<=24; j++){
				jj = j;
				if(j < 10)
					jj = 0+String(jj);
				$('.ryad'+ii).append('<span class="place" id="r'+ii+'m'+jj+'"><p>'+j+'</p></span>');
			}
		else if(i==3 || i==11)
			for(j=1; j<=26; j++){
				jj = j;
				if(j < 10)
					jj = 0+String(jj);
				$('.ryad'+ii).append('<span class="place" id="r'+ii+'m'+jj+'"><p>'+j+'</p></span>');
			}
		else if(i>=4 && i<=10 || i>=13 && i<=14)
			for(j=1; j<=28; j++){
				jj = j;
				if(j < 10)
					jj = 0+String(jj);
				$('.ryad'+ii).append('<span class="place" id="r'+ii+'m'+jj+'"><p>'+j+'</p></span>');
			}
		else for(j=1; j<=30; j++){
			jj = j;
			if(j < 10)
				jj = 0+String(jj);
			$('.ryad'+ii).append('<span class="place" id="r'+ii+'m'+jj+'"><p>'+j+'</p></span>');
		}
		$('.places-wrapper').append('</div></div>');
	}
}

function sendBron(){//Бронирование
	rand = randomInteger(100000, 999999);
	var places = Array();
	$.each($('.selectedPlace'), function(index, value){
		places[index] = $('.selectedPlace').eq(index).attr('id');
	});
	var phpPlaces = JSON.stringify(places);
	var name = $('#bronName').val();
	var mail = $('#bronMail').val();
	var sessionID = $('#bronSessionTime').val();
	var filmID = $('#bronFilmName').val();
	$.ajax({//Загрузим время
		url: "php/bron.php",
		type: "GET",
		data: ({key: "sendBron", ses_id: sessionID, identificator: rand, name: name, mail: mail, places: phpPlaces, film_id: filmID}),
		dataType: "html",
		
		success: function(data){
			if(data == true){
				$('.selectedPlace').addClass('BronnedPlace');
				$('.selectedPlace').removeClass('selectedPlace');
				AllCost = 0;
				$('.bronCena').html(AllCost);
				$('.nomerBroni').text('');
				$('.nomerBroni').append(rand);
				$('#successPlaces').text('');
				for(var id in places){
					$('#successPlaces').append('<li>Ряд '+places[id].substr(1,2)+', место '+places[id].substr(4,2)+'</li>');
				}
				$('.parent').show(0);
			}
		}
	});
}

function loadBronnedPlaces(){//Загрузка занятых мест
	$('.BronnedPlace').removeClass('BronnedPlace');//Снимаю забронированные места
	$.ajax({
		url: "php/bron.php",
		type: "GET",
		data: ({key: "loadBronnedPlaces", id: $("#bronSessionTime").val()}),
		dataType: "html",
		
		success: function(data){
			//$('body').append(data);
			data = JSON.parse(data);
			for(var id in data){
				$('#'+data[id]).addClass('BronnedPlace');
			}
		}
	});
}

function loadTime(){//Загрузка времени сеансов на этот день
	$('#bronSessionTime').empty();
	$('#bronSessionTime').append('<option value="loading">loading</option>');
	$.ajax({//Загрузим время
		url: "php/bron.php",
		type: "GET",
		data: ({key: "loadTime", id: $("#bronFilmName").val(), dat: $("#bronSessionDate").val()}),
		dataType: "html",
		
		success: function(data){
			$('#bronSessionTime').empty();
			data = JSON.parse(data);
			for(var id in data){
				$('#bronSessionTime').append("<option value='"+id+"'>"+data[id]+"</option>");
			}
			loadBronnedPlaces();
		}
	});
}

function loadData(){//Загрузка даты сеансов для этого фильма
	$('#bronSessionDate').empty();
	$('#bronSessionDate').append('<option value="loading">loading</option>');
	$('#bronSessionTime').empty();
	$('#bronSessionTime').append('<option value="loading">loading</option>');
	$.ajax({
		url: "php/bron.php",
		type: "GET",
		data: ({key: "loadData", id: $("#bronFilmName").val()}),
		dataType: "html",
		
		success: function(data){
			$('#bronSessionDate').empty();
			data = JSON.parse(data);
			for(var id in data){
				$('#bronSessionDate').append("<option value='"+id+"'>"+data[id]+"</option>")
			}
			loadTime();
		}
	});
}

window.onload = function(){
	$( "#bronFilmName" ).change(function() {//Если изменяется выбранный фильм
		loadData();
		$.ajax({//Грузим цену фильма
			url: "php/bron.php",
			type: "GET",
			data: ({key: "loadCost", id: $("#bronFilmName").val()}),
			dataType: "html",
			
			success: function(data){
				CenaFilma = data;
			}
		});
	});

	$.ajax({//Грузим цену фильма
		url: "php/bron.php",
		type: "GET",
		data: ({key: "loadCost", id: $("#bronFilmName").val()}),
		dataType: "html",
		
		success: function(data){
			CenaFilma = data;
		}
	});

	$(".place").click(function() {//Выделение мест
		if($(this).hasClass('selectedPlace')){
			$(this).removeClass('selectedPlace');
			AllCost -= Number(CenaFilma);
		} else if(!$(this).hasClass('BronnedPlace')){
			$('#'+$(this).attr('id')).addClass('selectedPlace');
			AllCost += Number(CenaFilma);
		}
		$('.bronCena').html(AllCost);
	});

	
	loadData();//Первая загрузка страницы: загрузка даты -> времени

	$( "#bronSessionDate" ).change(function() {//Если изменяется дата
		$('.BronnedPlace').removeClass('BronnedPlace');//Снимаю забронированные места
		loadTime();
	});

	$( "#bronSessionTime" ).change(function() {//Если изменяется время
		$('.BronnedPlace').removeClass('BronnedPlace');//Снимаю забронированные места
		loadBronnedPlaces();
	});

}

function valid_sendBron(){
    $('.BronContacts i').detach();
    var name = 0;
    var email = 0;
    var places = 0;
    if($('#bronName').val() == '') {
        $('.bronNameContainer').append('<i class="fa fa-times fa-2x image" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.bronNameContainer').append('<i class="fa fa-check fa-2x image" aria-hidden="true" style="color: green"></i>');
    	name = 1;
    }
    var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var mail = $('#bronMail');
    if(mail.val().search(pattern) == -1){
        $('.bronMailContainer').append('<i class="fa fa-times fa-2x image" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.bronMailContainer').append('<i class="fa fa-check fa-2x image" aria-hidden="true" style="color: green"></i>');
    	email = 1;
    }

    if($('.selectedPlace').length > 0)
    	places = 1;
    else alert("Выберите хотя бы одно место!");

	$('.bronNameContainer').fadeIn(1000);
	$('.bronMailContainer').fadeIn(1000);
    if(name == 1 && email == 1 && places == 1){
        sendBron();
    }
}