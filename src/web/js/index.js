function valid_sendMail(){
    $('.nameValid').hide();
    $('.mailValid').hide();
    $('.textValid').hide();
    $('.nameValid').html('');
    $('.mailValid').html('');
    $('.textValid').html('');
    var name = 0;
    var email = 0;
    var message = 0;
    if($('#name').val() == '') {
        $('.nameValid').append('<i class="fa fa-times fa-2x" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.nameValid').append('<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>');
    	name = 1;
    }
    if($('#message').val() == '') {
        $('.textValid').append('<i class="fa fa-times fa-2x" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.textValid').append('<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>');
    	message = 1;
    }
   
    var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var mail = $('#mail');
    if(mail.val().search(pattern) == -1){
        $('.mailValid').append('<i class="fa fa-times fa-2x" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.mailValid').append('<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>');
    	email = 1;
    }

    if(name == 1 && email == 1 && message == 1){
        $('#send').submit();
    }else{
        $('.nameValid').fadeIn(1000);
	    $('.mailValid').fadeIn(1000);
	    $('.textValid').fadeIn(1000);
    }
}

window.onload = function(){
	$('.carousel-indicators li').first().addClass("active");//Выставить активной первую точку в карусели
    $('.item').first().addClass("active");
}