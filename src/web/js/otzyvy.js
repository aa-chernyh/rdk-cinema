function valid_sendComment(){
    $('.nameCommentValid').hide();
    $('.commentValid').hide();
    $('.nameCommentValid').html('');
    $('.commentValid').html('');
    var nameComment = 0;
    var comment = 0;
    if($('.nameComment').val() == '') {
        $('.nameCommentValid').append('<i class="fa fa-times fa-2x" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.nameCommentValid').append('<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>');
    	nameComment = 1;
    }
    if($('.commentText').val() == '') {
        $('.commentValid').append('<i class="fa fa-times fa-2x" style="color: red" aria-hidden="true"></i>');
    }
    else{
    	$('.commentValid').append('<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>');
    	comment = 1;
    }

    if(nameComment == 1 && comment == 1){
        $('#sendComment').submit();
    }else{
        $('.nameCommentValid').fadeIn(1000);
	    $('.commentValid').fadeIn(1000);
    }
}