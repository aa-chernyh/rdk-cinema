var countDatesSession = 1;

function film(){
	var name = $('#addFilmName').val();
	var v_img = $('#addFilmImgV').val();
	var h_img = $('#addFilmImgH').val();
	var zhanr = $('#addFilmZhanr').val();
	var nachalo = $('#addFilmNachalo').val();
	var konec = $('#addFilmKonec').val();
	var desc = $('#addFilmDescription').val();
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "addfilm", name: name, v_img: v_img, h_img: h_img, zhanr: zhanr, nachalo: nachalo, konec: konec, desc: desc}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Фильм добавлен!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

function changefilm(){
	var name = $('#changeFilm').val();
	var v_img = $('#changeFilmImgV').val();
	var h_img = $('#changeFilmImgH').val();
	var zhanr = $('#changeFilmZhanr').val();
	var nachalo = $('#changeFilmNachalo').val();
	var konec = $('#changeFilmKonec').val();
	var desc = $('#changeFilmDescription').val();
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "changefilm", name: name, v_img: v_img, h_img: h_img, zhanr: zhanr, nachalo: nachalo, konec: konec, desc: desc}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Фильм изменен!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

function closeMessage(){
	$('.message-wrap').fadeOut();
}

function delFilm(){
	var name = $('#deleteFilmName').val();
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "delfilm", id: name}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Фильм удален!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

function delOldFilms(){
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "deloldfilms"}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Фильмы удалены!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

function addsession(){
	var name = $('#addSessionName').val();
	var data = "";
	var Ch = $('#addSessionVremyaCh').val();
	var M = $('#addSessionVremyaM').val();
	var cena = $('#addSessionCena').val();
	var space = $('#addSessionSpace').val();
	var vremya = Ch+":"+M+":00";
	for(i=0; i < countDatesSession; i++){
		data = $('#addSessionDate'+(i+1)).val();
		$.ajax({
			url: "php/cms.php",
			type: "GET",
			data: ({key: "addsession", id: name, data: data, vremya: vremya, cena: cena, space: space}),
			dataType: "html",
			error: function(xhr, status, error) {
				alert(xhr.responseText + '|\n' + status + '|\n' +error);
			},
			success: function(data){
						$('.message-wrap').fadeOut();
						$('.message').empty();
						if(data == 1)
							$('.message').append('<p style="color: green">Сеанс добавлен!</p>');
						else $('.message').append('<p style="color: red">Ошибка!</p>');
						$('.message-wrap').fadeIn();
						setTimeout('location.replace("cms.php");', 3000);
					}
		});
	}
}

function delsession(){
	var vremya = $('#delSessionVremya').val();
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "delsession", id: vremya}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Сеанс удален!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

function delOldsessions(){
	var vremya = $('#delSessionVremya').val();
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "deloldsessions"}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('.message-wrap').fadeOut();
					$('.message').empty();
					if(data == 1)
						$('.message').append('<p style="color: green">Сеансы удалены!</p>');
					else $('.message').append('<p style="color: red">Ошибка!</p>');
					$('.message-wrap').fadeIn();
					setTimeout('location.replace("cms.php");', 3000);
				}
	});
}

window.onload = function(){
	$( "#delSessionName" ).change(function() {
		$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "loadSession", id: $("#delSessionName").val()}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('#delSessionVremya').empty();
					data = JSON.parse(data);
					for(var id in data){
						$('#delSessionVremya').append("<option value='"+id+"'>"+data[id]+"</option>")
					}
				}
		});
	});
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "loadSession", id: $("#delSessionName").val()}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('#delSessionVremya').empty();
					data = JSON.parse(data);
					for(var id in data){
						$('#delSessionVremya').append("<option value='"+id+"'>"+data[id]+"</option>")
					}
				}
	});
	$( "#changeFilm" ).change(function() {
		$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "changeFilmById", id: $("#changeFilm").val()}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					text = data.split(',;,');
					$('#changeFilmImgV').val(text[0]);
					$('#changeFilmImgH').val(text[1]);
					$('#changeFilmZhanr').val(text[2]);
					$('#changeFilmNachalo').val(text[3]);
					$('#changeFilmKonec').val(text[4]);
					$('#changeFilmDescription').val(text[5]);
				}
		});
	});
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "changeFilmById", id: $("#changeFilm").val()}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					text = data.split(',;,');
					$('#changeFilmImgV').val(text[0]);
					$('#changeFilmImgH').val(text[1]);
					$('#changeFilmZhanr').val(text[2]);
					$('#changeFilmNachalo').val(text[3]);
					$('#changeFilmKonec').val(text[4]);
					$('#changeFilmDescription').val(text[5]);
				}
		});
}

function comment_ok(id, status){
	$.ajax({
		url: "php/cms.php",
		type: "GET",
		data: ({key: "comment", id: id, status: status, otvet: $("#otvet"+id).val()}),
		dataType: "html",
		error: function(xhr, status, error) {
			alert(xhr.responseText + '|\n' + status + '|\n' +error);
		},
		success: function(data){
					$('#tr'+id).remove();
				}
	});
}

function createNewDate(){
	countDatesSession += 1;
	$('.Dates').append('<p><input type="text" name="data" class="tcal" id="addSessionDate'+countDatesSession+'"></p>');
	$('.dateNames').append('<p>Дата '+countDatesSession+':</p>')
}