﻿function filter_reset(){
	$('#datafilter').val('');
	$('#cena_ot').val('');
	$('#cena_do').val('');
	$('#zhanr').val('Любой');
	$('#space').val('Любое');
	$('#filmName').val('Любой');
	$('#formFilter').submit();
}

function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
	"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		  ));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

window.onload = function(){
	if(getCookie('zhanr') != undefined){
		$('#zhanr').val(getCookie('zhanr'));
	}
	if(getCookie('space') != undefined){
		$('#space').val(getCookie('space'));
	}
	if(getCookie('filmName') != undefined){
		$('#filmName').val(getCookie('filmName'));
	}
	if($('.deleteTR').length == 0){
		$('.table table').append("<tr><td colspan='6'>Сеансов не найдено!</td></tr>");
	}
}