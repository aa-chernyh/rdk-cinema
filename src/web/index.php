<?php
	$mysqli = new mysqli("localhost", "rdknikpe_rdk", "rdkrdk", "rdknikpe_rdk");
	$mysqli->query("SET NAMES 'utf8'");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Главная - Кинозал РДК</title>
	<link href="img/favicon.ico" rel="icon" type="image/x-icon">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="js/index.js"></script>
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
	<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="css/index.css">
	<script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>
</head>
<body>
	<div class="row top-row">
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</div>
		<div class="col-md-6 top-text col-xs-12"><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</div>
		<div class="col-md-3 top-text col-xs-12"><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></div>
	</div>
	<div class="row">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="http://кино-рдк.рф"><i class="fa fa-film" aria-hidden="true"></i> Кинозал РДК</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="http://кино-рдк.рф">Главная</a></li>
						<li><a href="http://кино-рдк.рф/расписание">Расписание сеансов</a></li>
						<li><a href="http://кино-рдк.рф#soon">Скоро в кино</a></li>
						<li><a href="http://кино-рдк.рф/цены">Цены билетов</a></li>
						<li><a href="http://кино-рдк.рф/план">План зала</a></li>
						<li><a href="http://кино-рдк.рф/правила">Правила</a></li>
						<li><a href="http://кино-рдк.рф/отзывы">Отзывы</a></li>
					</ul>
					<form class="navbar-form navbar-right" action="cms" method="POST">
						<div class="form-group">
							<!-- Modal -->
							<div id="myModal" class="modal fade" role="dialog">
							  <div class="modal-dialog">
							    <!-- Modal content-->
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Вход в систему управления контентом</h4>
							      </div>
							      <div class="modal-body">
							        <input name="login" type="text" placeholder="login" class="form-control">
									<input name="pass" type="password" placeholder="password" class="form-control">
							      </div>
							      <div class="modal-footer">
							      	<button autofocus type="button" class="btn btn-default" onclick="$('.navbar-form').submit();">Войти</button>
							        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
							      </div>
							    </div>

							  </div>
							</div>
						</div>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<li><button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#myModal">CMS</button></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	<div class="row slider">
		<!-- Карусель -->
		<div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php
				$names = Array();
				$result = $mysqli->query("SELECT `name` FROM `current_films` WHERE `nachalo` <= CURDATE( ) AND `konec` >= CURDATE()");
				for($i=0; $row = $result->fetch_assoc(); $i++){
					$names[$i] = $row['name'];
					echo "<li data-target=\"#myCarousel\" data-slide-to=\"".$i."\" onclick=\"carPointClick(".$i.")\"></li>";
				}
				?>
			</ol>   
			<!-- Слайды карусели -->
			<div class="carousel-inner">
				<?php
					$result = $mysqli->query("SELECT `horizontal_img` FROM `current_films` WHERE `nachalo` <= CURDATE( ) AND `konec` >= CURDATE()");
					for($i=0; $row = $result->fetch_assoc(); $i++){
				?>
				<div class="item">
					<img src="img/carousel/<?=$row['horizontal_img']?>">
					<div class="carousel-caption">
						<h2><?=$names[$i]?></h2>
						<p>Кинозал Никифоровского ДК</p>
					</div>
				</div>
				<?php } ?>
				<a class="carousel-control left" onclick="leftClick()" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
		<div class="row">
			<h1 class="big">Сейчас в кинозале дома культуры:</h1>
		</div>
		<div class="row announces">
			<div class="col-md-1"></div>
			<div class="wrapper col-md-10">
				<?php
				$result = $mysqli->query("SELECT `vertical_img`, `description`, `id` FROM `current_films` WHERE `nachalo` <= CURDATE( ) AND `konec` >= CURDATE()");
				for($i=1, $j=0; $row = $result->fetch_assoc(); $i++, $j++){
					if((($i%5) == 0) && ($i !=1)){
						$i=1
						?>
					</div>
					<div class="col-md-1"></div>
				</div>
				<div class="row announces">
					<div class="col-md-1"></div>
					<div class="wrapper col-md-10">
						<?php
					}
					?>
					<div class="announce col-md-3 col-sm-12">
					<div class="an-picture">
						<div class="absDescription film<?=$j?>" onmouseout="$('.film<?=$j?>').fadeOut(200)">
							<p><?=$row['description']?></p>
						</div>
						<img src="img/<?=$row['vertical_img']?>" alt="<?=$names[$j]?>" title="<?=$names[$j]?>" onmouseover="$('.film<?=$j?>').fadeIn(200)">
					</div>
					<div class="sessionsToday film<?=$j?>">
						<p>Сеансы сегодня:</p>
						<table>
							<tr>
								<?php
									$results = $mysqli->query("SELECT CAST(`vremya` AS CHAR(5)) FROM `sessions` WHERE `film_id`=".$row['id']." AND `data` = CURDATE()");
									$insert = 0;
									while($rows = $results->fetch_assoc()){
										?>
										<td><?=$rows["CAST(`vremya` AS CHAR(5))"]?></td>
										<?php
										$insert = 1;
									}
									if($insert == 0){
										?>
										<td>Нет</td>
										<?
									}
								?>
							</tr>
						</table>
					</div>
				</div>
				<?php
			}
			?>
		</div>
		<div class="col-md-1"></div>
	</div>
	<div class="soon" id="soon">
		<div class="row">
			<h2 class="big">Скоро в кино:</h2>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<?php
				$result = $mysqli->query("SELECT `nachalo`, `vertical_img`, `name` FROM `current_films` WHERE `nachalo` > CURDATE() ORDER BY `nachalo`");
				for($i=0; $row = $result->fetch_assoc(); $i++){
					if((($i%3) == 0) && ($i != 0)){
						?>
					</div>
					<div class="col-md-1"></div>
				</div>
				<div class="row">
					<div class="col-md-1"></div>
					<div class="wrapper col-md-10">
						<?php
					}
					?>
					<div class="img-soon col-md-4 col-sm-12">
						<span class="soonTime"><?=DateTime::createFromFormat('Y-m-d', $row['nachalo'])->format('d.m.Y');?></span>
						<img src="img/<?=$row['vertical_img']?>" alt="<?=$row['name']?>" title="<?=$row['name']?>">
					</div>
					<?php
				}
				?>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	<div class="row bottom-row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="col-md-4">
				<!-- VK Widget -->
				<div id="vk_groups"></div>
				<script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 0, width: "auto"}, 158350900);
				</script>
			</div>
			<div class="col-md-4 contacts col-sm-12">
				<p class="footer-H">Контакты</p>
				<ul>
					<li><i class="fa fa-phone" aria-hidden="true"></i> +7(475)36 31-7-15</li>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i> Тамбовская область, Никифоровский район, р.п.Дмитриевка пл.Ленина д.6</li>
					<li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:rdk-nikiforowka@yandex.ru">rdk-nikiforowka@yandex.ru</a></li>
				</ul>
			</div>
			<div class="col-md-4 feedback col-sm-12">
				<form action="php/sendMail.php" method="POST" id="send">
					<p class="footer-H">Напишите нам</p>
					<ul>
						<li>
							<input type="text" name="name" placeholder="Имя" id="name" class="formInput">
							<span class="nameValid"></span>
						</li>
						<li>
							<input type="text" name="mail" placeholder="E-mail" id="mail" class="formInput">
							<span class="mailValid"></span>
						</li>
						<li>
							<textarea name="message" id="message" placeholder="Сообщение" class="formInput"></textarea>
							<span class="textValid"></span>
						</li>
					</ul>
					<input type="button" value="ОТПРАВИТЬ" class="submit" onclick="valid_sendMail()">
				</form>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>
</body>
</html>
<?php
	$mysqli->close();
?>