#include "reports.h"

reports::reports()
{
    reportsWidget = new QWidget;
        makeReportWidget = new QWidget;
            mainLayer = new QHBoxLayout;
                makeReport = new QVBoxLayout;
                    make = new QLabel(tr("Сформировать отчет из:"));
                        make->setStyleSheet("font-weight: bold;");
                        make->setAlignment(Qt::AlignCenter);
                    cols = new QVBoxLayout;
                        col1 = new QComboBox;
                            col1->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
                            nameOfCols << "Выбрать столбец"
                                       << "Название фильма"
                                       << "Вертикальная картинка"
                                       << "Горизонтальная картинка"
                                       << "Описание фильма"
                                       << "Начало проката"
                                       << "Окончание проката"
                                       << "Жанр фильма"
                                       << "Время сеанса"
                                       << "Дата сеанса"
                                       << "Цена"
                                       << "Пространство"
                                       << "Идентификатор брони"
                                       << "Дата и время бронирования"
                                       << "Имя забронировавшего"
                                       << "Почта забронировавшего"
                                       << "Место в зале"
                                       << "Дней проката";
                            col1->addItems(nameOfCols);
                        col2 = new QComboBox;
                            col2->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
                            col2->addItems(nameOfCols);
                    addCol = new QPushButton(tr("Добавить столбец"));
                        addCol->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                        connect(addCol, SIGNAL(clicked(bool)), SLOT(addNewCol()));
                    showReport = new QPushButton(tr("Показать отчет"));
                        showReport->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                        connect(showReport, SIGNAL(clicked(bool)), SLOT(createQuery()));
            standartReportsWidget = new QWidget;
                standartReports = new QVBoxLayout;
                    standart = new QLabel(tr("Типовые отчеты"));
                        standart->setStyleSheet("font-weight: bold;");
                        standart->setAlignment(Qt::AlignCenter);
                    films_report = new QPushButton(tr("Отчет по фильмам"));
                        films_report->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                        connect(films_report, SIGNAL(clicked(bool)), SLOT(showF_report()));
                    booking_report = new QPushButton(tr("Отчет по бронированию"));
                        booking_report->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                        connect(booking_report, SIGNAL(clicked(bool)), SLOT(showT_report()));

    cols->addWidget(col1);
    cols->addWidget(col2);
        makeReport->addWidget(make);
        makeReport->addLayout(cols);
        makeReport->addWidget(addCol);
        makeReport->addWidget(showReport);
            makeReportWidget->setLayout(makeReport);
            makeReportWidget->setMaximumHeight(200);

    standartReports->addWidget(standart);
    standartReports->addWidget(films_report);
    standartReports->addWidget(booking_report);
        standartReportsWidget->setLayout(standartReports);
            standartReportsWidget->setFixedHeight(135);
            mainLayer->addWidget(makeReportWidget);
            mainLayer->addWidget(standartReportsWidget);
                reportsWidget->setLayout(mainLayer);
                //reportsWidget->setStyleSheet("background-color: #FFFFFF");
}

void reports::addNewCol()
{
    switch(currentColCount)
    {
    case 2:
        col3 = new QComboBox;
        col3->addItems(nameOfCols);
        col3->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col3);
        currentColCount++;
        makeReportWidget->setMaximumHeight(250);
        break;
    case 3:
        col4 = new QComboBox;
        col4->addItems(nameOfCols);
        col4->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col4);
        currentColCount++;
        makeReportWidget->setMaximumHeight(300);
        break;
    case 4:
        col5 = new QComboBox;
        col5->addItems(nameOfCols);
        col5->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col5);
        currentColCount++;
        makeReportWidget->setMaximumHeight(350);
        break;
    case 5:
        col6 = new QComboBox;
        col6->addItems(nameOfCols);
        col6->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col6);
        currentColCount++;
        makeReportWidget->setMaximumHeight(400);
        break;
    case 6:
        col7 = new QComboBox;
        col7->addItems(nameOfCols);
        col7->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col7);
        currentColCount++;
        makeReportWidget->setMaximumHeight(450);
        break;
    case 7:
        col8 = new QComboBox;
        col8->addItems(nameOfCols);
        col8->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col8);
        currentColCount++;
        makeReportWidget->setMaximumHeight(500);
        break;
    case 8:
        col9 = new QComboBox;
        col9->addItems(nameOfCols);
        col9->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col9);
        currentColCount++;
        makeReportWidget->setMaximumHeight(550);
        break;
    case 9:
        col10 = new QComboBox;
        col10->addItems(nameOfCols);
        col10->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col10);
        currentColCount++;
        makeReportWidget->setMaximumHeight(600);
        break;
    case 10:
        col11 = new QComboBox;
        col11->addItems(nameOfCols);
        col11->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col11);
        currentColCount++;
        makeReportWidget->setMaximumHeight(650);
        break;
    case 11:
        col12 = new QComboBox;
        col12->addItems(nameOfCols);
        col12->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col12);
        currentColCount++;
        makeReportWidget->setMaximumHeight(700);
        break;
    case 12:
        col13 = new QComboBox;
        col13->addItems(nameOfCols);
        col13->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col13);
        currentColCount++;
        makeReportWidget->setMaximumHeight(750);
        break;
    case 13:
        col14 = new QComboBox;
        col14->addItems(nameOfCols);
        col14->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col14);
        currentColCount++;
        makeReportWidget->setMaximumHeight(800);
        break;
    case 14:
        col15 = new QComboBox;
        col15->addItems(nameOfCols);
        col15->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0");
        cols->addWidget(col15);
        currentColCount++;
        makeReportWidget->setMaximumHeight(850);
        break;
    default: break;
    }
}

QString reports::headerNames(QString value)
{
    if(value == "Название фильма"){
        films_used = true;
        return "`current_films`.`name` AS 'film_name'";
    }
    if(value == "Вертикальная картинка"){
        films_used = true;
        return "`current_films`.`vertical_img`";
    }
    if(value == "Горизонтальная картинка"){
        films_used = true;
        return "`current_films`.`horizontal_img`";
    }
    if(value == "Описание фильма"){
        films_used = true;
        return "`current_films`.`description`";
    }
    if(value == "Начало проката"){
        films_used = true;
        return "`current_films`.`nachalo`";
    }
    if(value == "Окончание проката"){
        films_used = true;
        return "`current_films`.`konec`";
    }
    if(value == "Жанр фильма"){
        films_used = true;
        return "`current_films`.`zhanr`";
    }
    if(value == "Время сеанса"){
        sessions_used = true;
        return "`sessions`.`vremya`";
    }
    if(value == "Дата сеанса"){
        sessions_used = true;
        return "`sessions`.`data`";
    }
    if(value == "Цена"){
        sessions_used = true;
        return "`sessions`.`cena`";
    }
    if(value == "Пространство"){
        sessions_used = true;
        return "`sessions`.`space`";
    }
    if(value == "Идентификатор брони"){
        bron_used = true;
        return "`bron`.`identificator`";
    }
    if(value == "Дата и время бронирования"){
        bron_used = true;
        return "`bron`.`datavremya`";
    }
    if(value == "Имя забронировавшего"){
        bron_used = true;
        return "`bron`.`name`";
    }
    if(value == "Почта забронировавшего"){
        bron_used = true;
        return "`bron`.`mail`";
    }
    if(value == "Место в зале"){
        bron_used = true;
        return "`bron`.`place`";
    }
    if(value == "Дней проката"){
        films_used = true;
        return "DATEDIFF(`current_films`.`konec`,`current_films`.`nachalo`) AS dney_prokata";
    }
    if(value == "Количество сеансов"){
        films_used = true;
        return "DATEDIFF(`current_films`.`konec`,`current_films`.`nachalo`) AS dney_prokata";
    }

    return "1";
}

void reports::createQuery()
{
    QString hvost = "";
    switch (currentColCount) {
    case 2:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText());
        break;
    case 3:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText());
        break;
    case 4:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText());
        break;
    case 5:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText());
        break;
    case 6:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText());
        break;
    case 7:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText());
        break;
    case 8:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText());
        break;
    case 9:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText());
        break;
    case 10:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText());
        break;
    case 11:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText()) + ", " + headerNames(col11->currentText());
        break;
    case 12:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText()) + ", " + headerNames(col11->currentText()) + ", " + headerNames(col12->currentText());
        break;
    case 13:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText()) + ", " + headerNames(col11->currentText()) + ", " + headerNames(col12->currentText()) + ", " + headerNames(col13->currentText());
        break;
    case 14:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText()) + ", " + headerNames(col11->currentText()) + ", " + headerNames(col12->currentText()) + ", " + headerNames(col13->currentText()) + ", " + headerNames(col14->currentText());
        break;
    case 15:
        querySQL = "SELECT " + headerNames(col1->currentText()) + ", " + headerNames(col2->currentText()) + ", " + headerNames(col3->currentText()) + ", " + headerNames(col4->currentText()) + ", " + headerNames(col5->currentText()) + ", " + headerNames(col6->currentText()) + ", " + headerNames(col7->currentText()) + ", " + headerNames(col8->currentText()) + ", " + headerNames(col9->currentText()) + ", " + headerNames(col10->currentText()) + ", " + headerNames(col11->currentText()) + ", " + headerNames(col12->currentText()) + ", " + headerNames(col13->currentText()) + ", " + headerNames(col14->currentText()) + ", " + headerNames(col15->currentText());
        break;
    default:
        break;
    }

    if(films_used && !sessions_used && !bron_used)
        hvost = " FROM `current_films`";
    if(!films_used && sessions_used && !bron_used)
        hvost = " FROM `sessions`";
    if(!films_used && !sessions_used && bron_used)
        hvost = " FROM `bron`";
    if(films_used && sessions_used && !bron_used)
        hvost = " FROM `current_films` , `sessions` WHERE `current_films`.`id` = `sessions`.`film_id`";
    if(!films_used && sessions_used && bron_used)
        hvost = " FROM `sessions` , `bron` WHERE `bron`.`ses_id` = `sessions`.`id`";
    if(films_used && !sessions_used && bron_used)
        hvost = " FROM `current_films`, `bron` WHERE `current_films`.`id` = `bron`.`film_id`";
    if(films_used && sessions_used && bron_used)
        hvost = " FROM `current_films` , `sessions` , `bron` WHERE `current_films`.`id` = `sessions`.`film_id` AND `bron`.`ses_id` = `sessions`.`id`";

    querySQL += hvost;

    //QMessageBox::question(this, "123", querySQL, QMessageBox::Yes|QMessageBox::No);

    sr = new show_report(querySQL, currentColCount);
    sr->show_reportWidget->show();

    films_used = false;
    sessions_used = false;
    bron_used = false;
}

void reports::showF_report()
{
    f_report fr;
    fr.f_reportWidget->show();
    fr.f_reportWidget->QWidget::showMaximized();
}

void reports::showT_report()
{
    t_report tr;
    tr.t_reportWidget->show();
    tr.t_reportWidget->QWidget::showMaximized();
}
