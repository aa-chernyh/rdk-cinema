#-------------------------------------------------
#
# Project created by QtCreator 2017-11-15T18:21:19
#
#-------------------------------------------------

QT       += core gui
QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    films.cpp \
    sessions.cpp \
    create_film.cpp \
    create_session.cpp \
    comments.cpp \
    new_comments.cpp \
    booking.cpp \
    booking_confirm.cpp \
    reports.cpp \
    show_report.cpp \
    f_report.cpp \
    t_report.cpp \
    entry.cpp

HEADERS += \
        mainwindow.h \
    films.h \
    sessions.h \
    create_film.h \
    create_session.h \
    comments.h \
    new_comments.h \
    booking.h \
    booking_confirm.h \
    reports.h \
    show_report.h \
    f_report.h \
    t_report.h \
    entry.h

FORMS += \
        mainwindow.ui

RESOURCES += \
    images.qrc
