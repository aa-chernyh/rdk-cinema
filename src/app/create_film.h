#ifndef CREATE_FILM_H
#define CREATE_FILM_H
#include <QDialog>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QTextEdit>
#include <QComboBox>
#include <QMessageBox>
#include <QTableWidget>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTabWidget>
#include <QHeaderView>


class create_film : public QDialog
{
    Q_OBJECT

public:
    create_film();
    QWidget *filmLayerWidget;
    QPushButton *addFilm;
private:

    QGridLayout *workArea;

    //textlines of workArea
    QLineEdit *nameFilmEdit;
    QComboBox *zhanrEdit;
    QLineEdit *vertImgEdit;
    QLineEdit *horImgEdit;
    QLineEdit *nachaloEdit;
    QLineEdit *konecEdit;
    QTextEdit *descriptionEdit;
    QPushButton *resetFilm;
    QPushButton *removeFilm;

public slots:
    void createFilm();
    void resetInputs();

};

#endif // CREATE_FILM_H
