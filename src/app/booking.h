#ifndef BOOKING_H
#define BOOKING_H
#include <QDialog>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QTableWidget>
#include <QHeaderView>
#include "booking_confirm.h"


class booking : public QDialog
{
    Q_OBJECT
public:
    booking();
    QWidget *bookingWidget;
private:
    QVBoxLayout *mainLayer;
        QVBoxLayout *layerID;
            QLineEdit *inputID;
            QPushButton *buttonID;
        QVBoxLayout *layerMail;
            QLineEdit *inputMail;
            QPushButton *buttonMail;

    QTableWidget *tableBronEdit = new QTableWidget;
    QVBoxLayout *tableBronLayer;
    QHBoxLayout *tableButtons;
        QPushButton *removeBron;
        QPushButton *payBron;

    booking_confirm *bk;
private slots:
    void searchBook();
    void createTable();
    void createTable(int sort);
    void removeRow();
    void okBook();
};

#endif // BOOKING_H
