#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QDesktopServices>
#include "films.h"
#include "sessions.h"
#include "create_film.h"
#include "comments.h"
#include "new_comments.h"
#include "booking.h"
#include "reports.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(int rules = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QHBoxLayout *mainLayer;
    QVBoxLayout *menu;
    QWidget *menuWidget;
    QWidget *mainWidget;

    //Кнопки меню
    QToolButton *films_button;
    QPushButton *sessions_button;
    QPushButton *comments_button;
    QPushButton *new_comments_button;
    QPushButton *booking_button;
    QPushButton *create_report_button;

    films *films_object;
    sessions *sessions_object;
    comments *comments_object;
    new_comments *newComments_object;
    booking *booking_object;
    reports *reports_object;

    QSqlDatabase db;
private slots:
    void about();
    void aboutAuthor();
    void goSite();

    void hideWidgets();
    void setFilmsLayer();
    void setSessionsLayer();
    void setCommentsLayer();
    void setNewCommentsLayer();
    void setBookingLayer();
    void setReportsLayer();
};

#endif // MAINWINDOW_H
