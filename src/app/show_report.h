#ifndef SHOW_REPORT_H
#define SHOW_REPORT_H
#include <QDialog>
#include <QWidget>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QSqlQuery>
#include <QHeaderView>
#include <QSqlRecord>


class show_report : public QDialog
{
    Q_OBJECT
public:
    show_report(QString querySQL, int cols);
    QWidget *show_reportWidget;
private:
    QVBoxLayout *mainLayer;
        QTableWidget *table;

    int countColumns;
    QString zaprosSQL;
    QStringList horizontalHeaders;
private slots:
    void createTable();
    void createTable(int sort);
    QString translate(QString value);
};

#endif // SHOW_REPORT_H
