#ifndef REPORTS_H
#define REPORTS_H
#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QComboBox>
#include <QStringList>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include "show_report.h"
#include "f_report.h"
#include "t_report.h"

class reports : public QDialog
{
    Q_OBJECT
public:
    reports();
    QWidget *reportsWidget;
private:
    QHBoxLayout *mainLayer;
        QWidget *makeReportWidget;
        QVBoxLayout *makeReport;
            QLabel *make;
            QVBoxLayout *cols;
                QComboBox *col1;
                QComboBox *col2;
                QComboBox *col3;
                QComboBox *col4;
                QComboBox *col5;
                QComboBox *col6;
                QComboBox *col7;
                QComboBox *col8;
                QComboBox *col9;
                QComboBox *col10;
                QComboBox *col11;
                QComboBox *col12;
                QComboBox *col13;
                QComboBox *col14;
                QComboBox *col15;
            QPushButton *addCol;
            QPushButton *showReport;
        QWidget *standartReportsWidget;
        QVBoxLayout *standartReports;
            QLabel *standart;
            QPushButton *films_report;
            QPushButton *booking_report;

    int currentColCount = 2; //Количество столбцов
    QStringList nameOfCols;
    QString querySQL;

    bool films_used = false; //Таблица фильмов была использована в запросе
    bool sessions_used = false;
    bool bron_used = false;

    show_report *sr;
private slots:
    void addNewCol();
    void createQuery();
    QString headerNames(QString value);

    void showF_report();
    void showT_report();
};

#endif // REPORTS_H
