#include "f_report.h"

f_report::f_report()
{
    createTable();

    //Слои
    f_reportLayer = new QVBoxLayout;
    f_reportWidget = new QWidget;
        f_reportWidget->setWindowTitle("Отчет по фильмам");
        f_reportWidget->setWindowIcon(QIcon(":/Images/otchety.jpg"));
    f_reportLayer->addWidget(tableF_report);
    f_reportWidget->setLayout(f_reportLayer);
    f_reportWidget->setMinimumWidth(793);
    f_reportWidget->setMinimumHeight(700);

}

void f_report::createTable()
{
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM current_films WHERE `nachalo` <= CURDATE()");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableF_report->clear();
    tableF_report->setColumnCount(4);
    tableF_report->setHorizontalHeaderLabels(QStringList() << "Название фильма" << "Конец проката" << "Продолжительность проката (дней)" << "Количество сеансов");
    tableF_report->setRowCount(rowsCount);
    tableF_report->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableF_report->resizeColumnsToContents();
    tableF_report->setColumnWidth(0, 540);

    query.exec("SELECT `id`, `name`, `konec`, DATEDIFF(`konec`, `nachalo`) FROM `current_films` WHERE `nachalo` <= CURDATE()");

    for(int i=0; query.next(); i++)
    {
        for(int j=0; j < 3; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableF_report->setItem(i, j, item);
        }

        //Количество сеансов
            QSqlQuery query2;
            query2.prepare("SELECT count(*)  FROM `sessions` WHERE `film_id` = :id");
            query2.bindValue(":id", query.value(0));
            qDebug() << query2.exec();
            query2.next();
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toInt()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableF_report->setItem(i, 3, item);
    }

    tableF_report->resizeColumnsToContents();
    tableF_report->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableF_report->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableF_report->verticalHeader()->setDefaultSectionSize(45);
    tableF_report->horizontalHeader()->setFixedHeight(45);
    for(int i=0; i<4; i++)
        tableF_report->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
}
