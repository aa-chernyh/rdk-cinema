#ifndef COMMENTS_H
#define COMMENTS_H
#include <QDialog>
#include <QGridLayout>
#include <QTableWidget>
#include <QSqlQuery>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include <QHeaderView>


class comments : public QDialog
{
    Q_OBJECT

public:
    comments();
    QWidget *commentsLayerWidget;

private:
    QTableWidget *tableComments = new QTableWidget;
    QVBoxLayout *tableCommentsLayer;

    QPushButton *removeComment;

    bool tableReset = 0;
public slots:
    void createTable();
    void createTable(int sort);
    void removeRow();
    void changeElem(int row,int column);
};

#endif // COMMENTS_H
