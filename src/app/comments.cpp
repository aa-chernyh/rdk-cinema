#include "comments.h"

comments::comments()
{
    createTable();

    //
    //Кнопки под таблицей
    //
    removeComment = new QPushButton(tr("Удалить комментарий (строку)"));
        removeComment->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(removeComment, SIGNAL(clicked()), SLOT(removeRow()));

    //
    //Слои
    //
    tableCommentsLayer = new QVBoxLayout;
        tableCommentsLayer->addWidget(tableComments);
        tableCommentsLayer->addWidget(removeComment);
    commentsLayerWidget = new QWidget;
        commentsLayerWidget->setLayout(tableCommentsLayer);

    //Изменение ячейки
    connect(tableComments, SIGNAL(cellChanged(int,int)), SLOT(changeElem(int,int)));

    //Реакция нажатия на столбцы (сортировка)
    connect(tableComments->horizontalHeader(), SIGNAL(sectionClicked(int)), SLOT(createTable(int)));
}

void comments::createTable()
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM comments WHERE `ok`='1'");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableComments->clear();
    tableComments->setColumnCount(3);
    tableComments->setHorizontalHeaderLabels(QStringList() << "Имя" << "Комментарий" << "Ответ");
    tableComments->setRowCount(rowsCount);
    tableComments->setColumnWidth(1, 237);
    tableComments->setColumnWidth(2, 237);

    query.exec("SELECT * FROM comments WHERE `ok`='1' ORDER BY `name`");

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();
        for(int j=0; j < 3; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableComments->setItem(i, j, item);
        }
    }
    tableComments->setVerticalHeaderLabels(VerticalHeaders);

    tableComments->resizeColumnsToContents();
    tableComments->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    tableComments->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    tableComments->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableComments->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableComments->verticalHeader()->setDefaultSectionSize(45);
    tableComments->horizontalHeader()->setFixedHeight(45);
    tableComments->verticalHeader()->hide();

    tableReset = 0;
}

void comments::createTable(int sort)
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM comments WHERE `ok`='1'");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableComments->clear();
    tableComments->setColumnCount(3);
    tableComments->setHorizontalHeaderLabels(QStringList() << "Имя" << "Комментарий" << "Ответ");
    tableComments->setRowCount(rowsCount);
    tableComments->setColumnWidth(1, 237);
    tableComments->setColumnWidth(2, 237);

    switch(sort){
    case 0: query.exec("SELECT * FROM comments WHERE `ok`='1' ORDER BY `name`"); break;
    case 1: query.exec("SELECT * FROM comments WHERE `ok`='1' ORDER BY `comment`"); break;
    case 2: query.exec("SELECT * FROM comments WHERE `ok`='1' ORDER BY `otvet`"); break;
    default: query.exec("SELECT * FROM comments WHERE `ok`='1' ORDER BY `name`");
    }

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();
        for(int j=0; j < 3; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableComments->setItem(i, j, item);
        }
    }
    tableComments->setVerticalHeaderLabels(VerticalHeaders);

    tableComments->resizeColumnsToContents();
    tableComments->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    tableComments->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    tableComments->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableComments->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableComments->verticalHeader()->setDefaultSectionSize(45);
    tableComments->horizontalHeader()->setFixedHeight(45);
    tableComments->verticalHeader()->hide();

    tableReset = 0;
}

void comments::removeRow()
{
    int com_id = tableComments->verticalHeaderItem(tableComments->currentRow())->text().toInt();

    QSqlQuery query;//Создаем объект запроса

    QMessageBox msg;
    msg.setWindowTitle("Удаление комментария");
    msg.setText("Удалить комментарий от " + tableComments->item(tableComments->currentRow(),0)->text() + "?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        tableComments->removeRow(tableComments->currentRow()); //удалили строчку
        query.prepare("DELETE FROM `kinonirb_rdk`.`comments` WHERE `comments`.`id` = :id;");
        query.bindValue(":id", com_id);
        qDebug() << query.exec();
    }

    /*QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, tableComments->item(tableComments->currentRow(),0)->text(), "Удалить комментарий?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        tableComments->removeRow(tableComments->currentRow()); //удалили строчку
        query.prepare("DELETE FROM `kinonirb_rdk`.`comments` WHERE `comments`.`id` = :id;");
        query.bindValue(":id", com_id);
        qDebug() << query.exec();
    }*/
}

void comments::changeElem(int row,int column)
{
    if(tableReset == 0){
        int com_id = tableComments->verticalHeaderItem(tableComments->currentRow())->text().toInt();

        QSqlQuery query;
        switch(column){
        case 0 : query.prepare("UPDATE `kinonirb_rdk`.`comments` SET `name`=:text WHERE `id`=:id"); break;
        case 1 : query.prepare("UPDATE `kinonirb_rdk`.`comments` SET `comment`=:text WHERE `id`=:id"); break;
        case 2 : query.prepare("UPDATE `kinonirb_rdk`.`comments` SET `otvet`=:text WHERE `id`=:id"); break;
        case 3 : query.prepare("UPDATE `kinonirb_rdk`.`comments` SET `ok`=:text WHERE `id`=:id"); break;
        default: return;
        }
        query.bindValue(":text", tableComments->item(row,column)->text());
        query.bindValue(":id", com_id);
        qDebug() << query.exec();
    }
}
