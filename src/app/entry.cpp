#include "entry.h"

entry::entry(QWidget *parent) :
    QMainWindow(parent)
{
    setWindowTitle("Вход в СУБД");
    setWindowIcon(QIcon(":/Images/logo.png"));
    setMinimumWidth(300);
    setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint);

    inputWidget = new QWidget;
        inputLayer = new QVBoxLayout;
            login = new QLineEdit;
                login->setPlaceholderText("login");
                login->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0; margin-left: 10px; margin-right: 10px; margin-top: 10px;");
            password = new QLineEdit;
                password->setEchoMode(QLineEdit::Password);
                password->setPlaceholderText("password");
                password->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0; margin-left: 10px; margin-right: 10px;");
            logIn = new QPushButton("Войти");
                logIn->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold; margin-left: 10px; margin-right: 10px; margin-bottom: 10px;");
                connect(logIn, SIGNAL(clicked(bool)), SLOT(enter()));
                logIn->setShortcut(Qt::Key_Return);


    inputLayer->addWidget(login);
    inputLayer->addWidget(password);
    inputLayer->addWidget(logIn);
        inputWidget->setLayout(inputLayer);
        inputWidget->setMaximumHeight(150);
            setCentralWidget(inputWidget);
}

void entry::enter()
{
    //
    /* База */
    //
    try//Пробую подключиться к базе данных и вывести таблицу
    {
        db1 = QSqlDatabase::addDatabase("QMYSQL3");
        db1.setHostName("localhost");
        db1.setDatabaseName("rdknikpe_rdk");
        db1.setUserName("rdknikpe_rdk");
        db1.setPassword("rdkrdk");
        bool ok = db1.open();
        qDebug() << ok;
        if(!ok)//Если не подключился к БД, вывожу сообщение
        {
            QMessageBox Warning;
            Warning.setWindowTitle(tr("Внимание!"));
            Warning.setText(tr("Нет подключения к базе данных!"));
            Warning.setWindowIcon(QIcon(":/Images/warning.png"));
            Warning.setIcon(QMessageBox::Warning);
            Warning.exec();
        }
    }
    catch(...)
    {

    }
    //
    /**/
    //

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT * FROM users");

    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(password->text().toLatin1());
    QString hashStr(hash.result().toHex());

    bool LogIn = false;
    for(int i=0; query.next(); i++)
    {
        if(login->text() == query.value(1) && hashStr == query.value(2))
        {
            w = new MainWindow(query.value(3).toInt());
            ok = true;
            w->show();
            LogIn = true;
            close();
        }
    }
    if(!query.next() && LogIn == false)
    {
        QMessageBox msg;
        msg.setWindowTitle("Ошибка");
        msg.setText("Неверный логин или пароль!");
        msg.setIcon(QMessageBox::Warning);
        msg.setWindowIcon(QIcon(":/Images/warning.png"));
        msg.exec();
    }
}
