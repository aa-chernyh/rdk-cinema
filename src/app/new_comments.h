#ifndef NEW_COMMENTS_H
#define NEW_COMMENTS_H
#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QPushButton>
#include <QSqlQuery>
#include <QDebug>


class new_comments : public QDialog
{
    Q_OBJECT

public:
    new_comments();
    QWidget *newCommentsWidget = new QWidget;

private:
    //слои и элементы
    QVBoxLayout *unMainLayer;
    QWidget *unMainWidget;
    QWidget *noCommentsWidget;
    QVBoxLayout *mainLayer;
        QLabel *name;
        QLabel *comment;
        QLabel *reply;
        QTextEdit *replyText;
        QHBoxLayout *buttons;
            QPushButton *ok;
            QPushButton *drop;
        QHBoxLayout *scroll;
            QPushButton *left;
            QLabel *page;
            QPushButton *right;

    int currentComment = -1;//Текущий комментарий
    int countComments = 0;//Количество комментариев

    QLabel *noComments;
    QHBoxLayout *noCommentsLayer;

    struct com//Сюда загружаются
    {
        int id;
        QString name;
        QString comment;
    }*comments;
private slots:
    int loadComments();
    void showComment(int number);
    void setOkTrue();
    void setOkFalse();
    void nextComment();
    void prewComment();
};

#endif // NEW_COMMENTS_H
