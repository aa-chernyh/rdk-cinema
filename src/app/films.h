#ifndef FILMS_H
#define FILMS_H
#include <QDialog>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QTextEdit>
#include <QComboBox>
#include <QMessageBox>
#include <QTableWidget>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlQuery>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTabWidget>
#include <QHeaderView>
#include "create_film.h"


class films : public QDialog
{
    Q_OBJECT

public:
    films();
    QWidget *filmLayerWidget;
    QTableWidget *table = new QTableWidget;
private:

    QVBoxLayout *tableLayer;
    QHBoxLayout *buttons;

    QPushButton *removeFilm;
    QPushButton *createFilmDialog;

    create_film *s = new create_film;

    bool tableReset = 0;//Используется для предотвращения вызова сигнала об изменении поля таблицы при её обновлении

public slots:
    void createTable();
    void createTable(int sort);
    void removeRow();
    void changeElem(int row,int column);
    void callNewFilm();

};

#endif // FILMS_H
