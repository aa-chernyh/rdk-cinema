#include "create_film.h"
#include "mainwindow.h"
#include "films.h"

create_film::create_film()
{

    //setStyleSheet("background-color: #FFFFFF");
    //
    //workArea
    //

    //Название
    QLabel *nameFilm = new QLabel("Название: ");
        nameFilm->setStyleSheet("font-weight: bold");
    nameFilmEdit = new QLineEdit;
        nameFilmEdit->setPlaceholderText(tr("Название фильма"));
        nameFilmEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Жанр
    QLabel *zhanr = new QLabel("Жанр: ");
        zhanr->setStyleSheet("font-weight: bold");
    zhanrEdit = new QComboBox;
        QStringList zhanry;
        zhanry << "Биографический"
               << "Боевик"
               << "Вестерн"
               << "Военный"
               << "Детектив"
               << "Документальный"
               << "Драма"
               << "Исторический"
               << "Кинокомикс"
               << "Комедия"
               << "Криминал"
               << "Мелодрама"
               << "Мистика"
               << "Музыка"
               << "Мультфильм"
               << "Мюзикл"
               << "Научный"
               << "Приключения"
               << "Семейный"
               << "Триллер"
               << "Ужасы"
               << "Фантастика"
               << "Фэнтези";
        zhanrEdit->addItems(zhanry);
        zhanrEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F8F8F8;");

    //Вертикальная картинка
    QLabel *vertImg = new QLabel("Вертикальная картинка: ");
        vertImg->setStyleSheet("font-weight: bold");
    vertImgEdit = new QLineEdit;
        vertImgEdit->setPlaceholderText(tr("/img/example.jpg"));
        vertImgEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Горизонтальная картинка
    QLabel *horImg = new QLabel("Горизонтльная картинка: ");
        horImg->setStyleSheet("font-weight: bold");
    horImgEdit = new QLineEdit;
        horImgEdit->setPlaceholderText(tr("/img/carousel/example.jpg"));
        horImgEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Начало показа
    QLabel *nachalo = new QLabel("Начало показа: ");
        nachalo->setStyleSheet("font-weight: bold");
    nachaloEdit = new QLineEdit;
        nachaloEdit->setPlaceholderText(tr("ГГГГ-ММ-ДД"));
        nachaloEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Конец показа
    QLabel *konec = new QLabel("Дней проката: ");
        konec->setStyleSheet("font-weight: bold");
    konecEdit = new QLineEdit;
        konecEdit->setPlaceholderText(tr("Дней"));
        konecEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Описание
    QLabel *description = new QLabel("Описание: ");
        description->setStyleSheet("font-weight: bold");
    descriptionEdit = new QTextEdit;

    //Кнопки
    addFilm = new QPushButton(tr("Добавить фильм"));
        addFilm->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        addFilm->setDefault(true);
        connect(addFilm, SIGNAL(clicked(bool)), SLOT(createFilm()));
        addFilm->setShortcut(Qt::Key_Return);
    resetFilm = new QPushButton(tr("Сбросить"));
        resetFilm->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(resetFilm, SIGNAL(clicked(bool)), SLOT(resetInputs()));

    //
    //Расположение (слои)
    //

    workArea = new QGridLayout;
        workArea->addWidget(nameFilm, 0,0);
        workArea->addWidget(nameFilmEdit, 1,0);
        workArea->addWidget(zhanr, 0,1);
        workArea->addWidget(zhanrEdit, 1,1);
        workArea->addWidget(vertImg, 2,0);
        workArea->addWidget(vertImgEdit, 3,0);
        workArea->addWidget(horImg, 2,1);
        workArea->addWidget(horImgEdit, 3,1);
        workArea->addWidget(nachalo, 4,0);
        workArea->addWidget(nachaloEdit, 5,0);
        workArea->addWidget(konec, 4,1);
        workArea->addWidget(konecEdit, 5,1);
        workArea->addWidget(description, 6,0);
        workArea->addWidget(descriptionEdit, 7,0, 1,2);
        workArea->addWidget(addFilm, 8,0);
        workArea->addWidget(resetFilm, 8,1);

        filmLayerWidget = new QWidget;
            filmLayerWidget->setLayout(workArea);
            filmLayerWidget->setWindowTitle(tr("Добавление нового фильма"));
            filmLayerWidget->setMinimumWidth(450);
            filmLayerWidget->setWindowIcon(QIcon(":/Images/Filmy.jpg"));

}

void create_film::createFilm(){
    QSqlQuery query;
    query.prepare("INSERT INTO `kinonirb_rdk`.`current_films` (`id`, `name`, `vertical_img`, `horizontal_img`, `description`, `nachalo`, `konec`, `zhanr`) "
                  "VALUES (NULL, :name, :v_img, :h_img, :desc, :nachalo, ADDDATE(:nachalo, :dney), :zhanr)");
    query.bindValue(":name", nameFilmEdit->text());
    query.bindValue(":v_img", vertImgEdit->text());
    query.bindValue(":h_img", horImgEdit->text());
    query.bindValue(":desc", descriptionEdit->toPlainText());
    query.bindValue(":nachalo", nachaloEdit->text());
    query.bindValue(":dney", konecEdit->text());
    query.bindValue(":zhanr", zhanrEdit->currentText());

    qDebug() << query.exec();

    this->filmLayerWidget->close();
}

void create_film::resetInputs()
{
    nameFilmEdit->clear();
    vertImgEdit->clear();
    horImgEdit->clear();
    descriptionEdit->clear();
    nachaloEdit->clear();
    konecEdit->clear();
    zhanrEdit->setCurrentIndex(0);
}

