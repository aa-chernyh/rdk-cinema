#include "films.h"
#include "mainwindow.h"

films::films()
{
    createTable();
    //
    //Кнопки под таблицей
    //
    removeFilm = new QPushButton(tr("Удалить фильм (строку)"));
        removeFilm->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(removeFilm, SIGNAL(clicked()), SLOT(removeRow()));
        removeFilm->setShortcut(Qt::Key_Delete);
    createFilmDialog = new QPushButton(tr("Добавить новый фильм"));
        createFilmDialog->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(createFilmDialog, SIGNAL(clicked(bool)), SLOT(callNewFilm()));


    //
    //Расположение (слои)
    //
    tableLayer = new QVBoxLayout;
    buttons = new QHBoxLayout;
        buttons->addWidget(removeFilm);
        buttons->addWidget(createFilmDialog);

    tableLayer->addWidget(table);
    tableLayer->addLayout(buttons);

    filmLayerWidget = new QWidget;
    filmLayerWidget->setLayout(tableLayer);


    //Изменение ячейки
    connect(table, SIGNAL(cellChanged(int,int)), SLOT(changeElem(int,int)));

    //Реакция на добавление фильма
    connect(s->addFilm, SIGNAL(clicked(bool)), this, SLOT(createTable()));

    //Реакция нажатия на столбцы (сортировка)
    connect(table->horizontalHeader(), SIGNAL(sectionClicked(int)), SLOT(createTable(int)));
}

void films::createTable()
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM current_films");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    table->clear();
    table->setColumnCount(7);
    table->setHorizontalHeaderLabels(QStringList() << "Название" << "Верт. картинка" << "Гориз. Картинка" << "Описание" << "Начало" << "Конец" << "Жанр");
    table->setRowCount(rowsCount);

    query.exec("SELECT * FROM current_films");

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();
        for(int j=0; j < 7; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            table->setItem(i, j, item);
        }
    }

    table->setVerticalHeaderLabels(VerticalHeaders);
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    table->resizeColumnsToContents();
    table->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    table->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    table->verticalHeader()->setDefaultSectionSize(45);
    table->horizontalHeader()->setFixedHeight(45);
    table->verticalHeader()->hide();

    tableReset = 0;
}

void films::createTable(int sort)
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM current_films");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    table->clear();
    table->setColumnCount(7);
    table->setHorizontalHeaderLabels(QStringList() << "Название" << "Верт. картинка" << "Гориз. Картинка" << "Описание" << "Начало" << "Конец" << "Жанр");
    table->setRowCount(rowsCount);


    switch(sort){
    case 0: query.exec("SELECT * FROM current_films ORDER BY `name`"); break;
    case 1: query.exec("SELECT * FROM current_films ORDER BY `vertical_img`"); break;
    case 2: query.exec("SELECT * FROM current_films ORDER BY `horizontal_img`"); break;
    case 3: query.exec("SELECT * FROM current_films ORDER BY `description`"); break;
    case 4: query.exec("SELECT * FROM current_films ORDER BY `nachalo`"); break;
    case 5: query.exec("SELECT * FROM current_films ORDER BY `konec`"); break;
    case 6: query.exec("SELECT * FROM current_films ORDER BY `zhanr`"); break;
    default: query.exec("SELECT * FROM current_films");
    }

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();
        for(int j=0; j < 7; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            table->setItem(i, j, item);
        }
    }

    table->setVerticalHeaderLabels(VerticalHeaders);
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    table->resizeColumnsToContents();
    table->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    table->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    table->verticalHeader()->setDefaultSectionSize(45);
    table->horizontalHeader()->setFixedHeight(45);
    table->verticalHeader()->hide();

    tableReset = 0;
}

void films::removeRow()
{
    int row = table->currentRow(); //нашли нужный номер строки

    //Ищем id'шник нужной строчки по имени, ибо удаление по имени удаляет некорректную строку.
    //Мб это косяк MySQL, но при удалении по имени
    //всегда удаляется строчка, находящаяся перед нужной, хотя
    //передаваемые аргументы верны (проверено)
    QSqlQuery query;//Создаем объект запроса
    query.prepare("SELECT `id` FROM `current_films` WHERE `name` = :name");
    query.bindValue(":name", table->item(table->currentRow(),0)->text());
    query.exec();
    query.next();
    int film_id = query.value(0).toInt();

    QMessageBox msg;
    msg.setWindowTitle(table->item(table->currentRow(),0)->text());
    msg.setText("Удалить фильм?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        table->removeRow(row); //удалили строчку
        query.prepare("DELETE FROM `kinonirb_rdk`.`current_films` WHERE `current_films`.`id` = :id;");
        query.bindValue(":id", film_id);
        qDebug() << query.exec();
    }
}

void films::changeElem(int row,int column)
{
    if(tableReset == 0){
        int film_id = table->verticalHeaderItem(table->currentRow())->text().toInt();
        QSqlQuery query;//Создаем объект запроса
        switch(column){
        case 0 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `name`=:text WHERE `id`=:id"); break;
        case 1 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `vertical_img`=:text WHERE `id`=:id"); break;
        case 2 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `horizontal_img`=:text WHERE `id`=:id"); break;
        case 3 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `description`=:text WHERE `id`=:id"); break;
        case 4 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `nachalo`=:text WHERE `id`=:id"); break;
        case 5 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `konec`=:text WHERE `id`=:id"); break;
        case 6 : query.prepare("UPDATE `kinonirb_rdk`.`current_films` SET `zhanr`=:text WHERE `id`=:id"); break;
        default: return;
        }
        query.bindValue(":text", table->item(row,column)->text());
        query.bindValue(":id", film_id);
        qDebug() << query.exec();
        createTable();
    }
}

void films::callNewFilm()
{
    s->filmLayerWidget->show();
}
