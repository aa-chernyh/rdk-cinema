#ifndef CREATE_SESSION_H
#define CREATE_SESSION_H

#include <QDialog>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QStringList>
#include <QLabel>
#include <QSqlQuery>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include <QHeaderView>
#include <QCheckBox>


class create_session : public QDialog
{
    Q_OBJECT

public:
    create_session();
    QWidget *sessionLayerWidget;
    QPushButton *addSession;

private:
    QVBoxLayout *sessionLayer;
        QTableWidget *tableSession = new QTableWidget;
        QGridLayout *workAreaSession;
        QVBoxLayout *tableSessionLayer;
        QHBoxLayout *workAreaSessionButtons;

    //textlines of workAreaSession
    QComboBox *nameFilmEdit;
    QLineEdit *dataEdit;
    QHBoxLayout *timeLayer;
        QComboBox *hh;
        QLabel *dvoetochie;
        QComboBox *mm;
    QLineEdit *cenaEdit;
    QComboBox *spaceEdit;

    QHBoxLayout *galochkaLayer;
        QCheckBox *galochka;
        QLabel *textLeft;
        QLineEdit *countDays;
        QLabel *textRight;


    QPushButton *resetSession;
    QPushButton *removeSession;
    bool tableReset = 0;

public slots:
    void createSession();
    void resetInputs();

private slots:
    void setFilmNames();

};


#endif // CREATE_SESSION_H
