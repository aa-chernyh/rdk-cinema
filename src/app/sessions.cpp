#include "sessions.h"
#include "films.h"


sessions::sessions()
{
    createTable();

    //Кнопки под таблицей
    removeSession = new QPushButton(tr("Удалить сеанс (строку)"));
        removeSession->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(removeSession, SIGNAL(clicked()), SLOT(removeRow()));
        removeSession->setShortcut(Qt::Key_Delete);
    createSessionDialog = new QPushButton(tr("Добавить сеанс(ы)"));
        createSessionDialog->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(createSessionDialog, SIGNAL(clicked(bool)), SLOT(callNewSession()));



    //
    //Слои
    //
    sessionLayer = new QGridLayout;

    tableSessionLayer = new QVBoxLayout;
        tableSessionLayer->addWidget(tableSession);

    tableButtons = new QHBoxLayout;
    tableButtons->addWidget(removeSession);
    tableButtons->addWidget(createSessionDialog);

    sessionLayer->addLayout(tableSessionLayer, 0,0);
    sessionLayer->addLayout(tableButtons, 1,0);

    sessionLayerWidget = new QWidget;
    sessionLayerWidget->setLayout(sessionLayer);

    //Изменение ячейки
    connect(tableSession, SIGNAL(cellChanged(int,int)), SLOT(changeElem(int,int)));

    //Реакция на добавление сеанса
    connect(s->addSession, SIGNAL(clicked(bool)), this, SLOT(createTable()));

    //Реакция нажатия на столбцы (сортировка)
    connect(tableSession->horizontalHeader(), SIGNAL(sectionClicked(int)), SLOT(createTable(int)));
}

void sessions::createTable()
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM sessions");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableSession->clear();
    tableSession->setColumnCount(5);
    tableSession->setHorizontalHeaderLabels(QStringList() << "Фильм" << "Время" << "Дата" << "Цена" << "Пространство" << "id Фильма");
    tableSession->setRowCount(rowsCount);

    query.exec("SELECT * FROM sessions ORDER BY `film_id`, `data`");

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();

        QSqlQuery query2;
        query2.prepare("SELECT `name` FROM `current_films` WHERE `id`=:id");
        query2.bindValue(":id", query.value(5));
        query2.exec();
        query2.next();
        QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toString()));
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        item->setFlags(Qt::ItemIsEditable^2);
        item->setTextColor(Qt::black);
        tableSession->setItem(i, 0, item);

        for(int j=0; j < 4; j++)
        {
            item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableSession->setItem(i, j+1, item);
        }
    }
    tableSession->setVerticalHeaderLabels(VerticalHeaders);

    tableSession->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableSession->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableSession->verticalHeader()->setDefaultSectionSize(45);
    tableSession->horizontalHeader()->setFixedHeight(45);
    tableSession->verticalHeader()->hide();

    tableReset = 0;
}

void sessions::createTable(int sort)
{
    tableReset = 1; //Предотвращаю вызов сигнала itemChanged

    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM sessions");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableSession->clear();
    tableSession->setColumnCount(5);
    tableSession->setHorizontalHeaderLabels(QStringList() << "Фильм" << "Время" << "Дата" << "Цена" << "Пространство" << "id Фильма");
    tableSession->setRowCount(rowsCount);

    switch(sort)
    {
    case 0: query.exec("SELECT * FROM sessions ORDER BY `film_id`"); break;
    case 1: query.exec("SELECT * FROM sessions ORDER BY `vremya`"); break;
    case 2: query.exec("SELECT * FROM sessions ORDER BY `data`"); break;
    case 3: query.exec("SELECT * FROM sessions ORDER BY `cena`"); break;
    case 4: query.exec("SELECT * FROM sessions ORDER BY `space`"); break;
    default: query.exec("SELECT * FROM sessions ORDER BY `film_id`, `data`");
    }

    QStringList VerticalHeaders;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();

        QSqlQuery query2;
        query2.prepare("SELECT `name` FROM `current_films` WHERE `id`=:id");
        query2.bindValue(":id", query.value(5));
        query2.exec();
        query2.next();
        QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toString()));
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        item->setFlags(Qt::ItemIsEditable^2);
        item->setTextColor(Qt::black);
        tableSession->setItem(i, 0, item);

        for(int j=0; j < 4; j++)
        {
            item = new QTableWidgetItem(tr("%1").arg(query.value(j+1).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            tableSession->setItem(i, j+1, item);
        }
    }
    tableSession->setVerticalHeaderLabels(VerticalHeaders);

    tableSession->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    tableSession->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableSession->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableSession->verticalHeader()->setDefaultSectionSize(45);
    tableSession->horizontalHeader()->setFixedHeight(45);
    tableSession->verticalHeader()->hide();

    tableReset = 0;
}

void sessions::removeRow()
{
    int row = tableSession->currentRow(); //нашли нужный номер строки
    int ses_id = tableSession->verticalHeaderItem(tableSession->currentRow())->text().toInt();

    QSqlQuery query;//Создаем объект запроса

    QMessageBox msg;
    msg.setWindowTitle("Удаление");
    msg.setText("Удалить сеанс фильма " + tableSession->item(tableSession->currentRow(), 0)->text() + " " + tableSession->item(tableSession->currentRow(), 2)->text() + " в " + tableSession->item(tableSession->currentRow(), 1)->text());
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        tableSession->removeRow(row); //удалили строчку
        query.prepare("DELETE FROM `kinonirb_rdk`.`sessions` WHERE `sessions`.`id` = :id;");
        query.bindValue(":id", ses_id);
        qDebug() << query.exec();
    }
}

void sessions::changeElem(int row,int column)
{
    if(tableReset == 0){
        QSqlQuery query;
        int ses_id = tableSession->verticalHeaderItem(tableSession->currentRow())->text().toInt();

        switch(column){
        case 0 : query.prepare("UPDATE `kinonirb_rdk`.`sessions` SET `vremya`=:text WHERE `id`=:id"); break;
        case 1 : query.prepare("UPDATE `kinonirb_rdk`.`sessions` SET `data`=:text WHERE `id`=:id"); break;
        case 2 : query.prepare("UPDATE `kinonirb_rdk`.`sessions` SET `cena`=:text WHERE `id`=:id"); break;
        case 3 : query.prepare("UPDATE `kinonirb_rdk`.`sessions` SET `space`=:text WHERE `id`=:id"); break;
        case 4 : query.prepare("UPDATE `kinonirb_rdk`.`sessions` SET `film_id`=:text WHERE `id`=:id"); break;
        default: return;
        }
        query.bindValue(":text", tableSession->item(row,column)->text());
        query.bindValue(":id", ses_id);
        qDebug() << query.exec();
    }
}

void sessions::callNewSession()
{
    s->sessionLayerWidget->show();
}
