#ifndef T_REPORT_H
#define T_REPORT_H
#include <QDialog>
#include <QVBoxLayout>
#include <QTableWidget>
#include <QSqlQuery>
#include <QDebug>
#include <QHeaderView>

class t_report : public QDialog
{
    Q_OBJECT
public:
    t_report();
    QWidget *t_reportWidget;
private:
    QVBoxLayout *t_reportLayer;
    QTableWidget *tableT_report = new QTableWidget;
public slots:
    void createTable();
};

#endif // T_REPORT_H
