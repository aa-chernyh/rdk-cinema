#include "new_comments.h"

new_comments::new_comments()
{
    countComments = loadComments();
    name = new QLabel;
        name->setStyleSheet("font-weight: bold;");
    comment = new QLabel;
        comment->setAlignment(Qt::AlignJustify);
        comment->setWordWrap(true);
    reply = new QLabel(tr("Ответить:"));
    reply->setStyleSheet("font-weight: bold;");
    replyText = new QTextEdit;
    buttons = new QHBoxLayout;
        ok = new QPushButton(tr("Подтвердить"));
            ok->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
            connect(ok, SIGNAL(clicked()), SLOT(setOkTrue()));
        drop = new QPushButton(tr("Отвергнуть"));
            drop->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
            connect(drop, SIGNAL(clicked(bool)), SLOT(setOkFalse()));
        buttons->addWidget(ok);
        buttons->addWidget(drop);
    scroll = new QHBoxLayout;
        left = new QPushButton("<");
            left->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-left: 170px;");
            left->setEnabled(false);
            connect(left, SIGNAL(clicked(bool)), SLOT(prewComment()));
        page = new QLabel;
            page->setAlignment(Qt::AlignCenter);
            page->setStyleSheet("font-size: 16px");
        right = new QPushButton(">");
            right->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold; margin-right: 170px;");
            connect(right, SIGNAL(clicked(bool)), SLOT(nextComment()));
            if(countComments == 1)
            {
                right->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-right: 170px;");
                right->setEnabled(false);
            }
        scroll->addWidget(left);
        scroll->addWidget(page);
        scroll->addWidget(right);

    currentComment = 0;
    if(countComments > 0)
        showComment(currentComment);

    unMainLayer = new QVBoxLayout;
        unMainLayer->addWidget(name);
        unMainLayer->addWidget(comment);
        unMainLayer->addWidget(reply);
        unMainLayer->addWidget(replyText);
        unMainLayer->addLayout(buttons);
        unMainLayer->addLayout(scroll);
    unMainWidget = new QWidget;
        unMainWidget->setLayout(unMainLayer);


    noComments = new QLabel(tr("Нет новых комментариев"));
        noComments->setAlignment(Qt::AlignCenter);
        noComments->setStyleSheet("font-weight: bold;");
    noCommentsLayer = new QHBoxLayout;
        noCommentsLayer->addWidget(noComments);
    noCommentsWidget = new QWidget;
        noCommentsWidget->setLayout(noCommentsLayer);

    mainLayer = new QVBoxLayout;
        mainLayer->addWidget(unMainWidget);
        mainLayer->addWidget(noCommentsWidget);
    newCommentsWidget->setLayout(mainLayer);

    if(countComments > 0){
        noCommentsWidget->hide();
    } else
    {
        unMainWidget->hide();
    }
}

int new_comments::loadComments()
{
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM `comments` WHERE `ok` = '0'");
    query.next();
    int rowsCount = query.value(0).toInt();
    comments = new com[rowsCount];

    query.exec("SELECT `id`, `name`, `comment` FROM `comments` WHERE `ok` = '0'");
    for(int i=0; query.next(); i++)
    {
        for(int j=0; j < 4; j++)
        {
            comments[i].id = query.value(0).toInt();
            comments[i].name = query.value(1).toString();
            comments[i].comment = query.value(2).toString();
        }
    }
    return rowsCount;
}

void new_comments::showComment(int number)
{
    name->setText(comments[number].name + tr(" пишет:"));
    comment->setText(comments[number].comment);
    page->setText(QString::number(currentComment+1) + tr(" из ") + QString::number(countComments));
    replyText->clear();
}

void new_comments::setOkTrue()
{
    QSqlQuery query;//Создаем объект запроса
    query.prepare("UPDATE `comments` SET `ok` = :result, `otvet` = :otvet WHERE `id` = :id");
    query.bindValue(":id", comments[currentComment].id);
    query.bindValue(":result", 1);
    query.bindValue(":otvet", replyText->toPlainText());
    qDebug() << query.exec();

    if(loadComments() != 0)
    {
        if(currentComment > 0 && currentComment < countComments-1)
        {
            countComments = loadComments();
            showComment(currentComment);
        }
        if(currentComment == 0)
        {
            countComments = loadComments();
            showComment(currentComment);
        }
        else if(currentComment == countComments-1)
        {
            countComments = loadComments();
            currentComment = countComments-1;
            showComment(currentComment);
        }
        countComments = loadComments();
    } else
    {
        unMainWidget->hide();
        noCommentsWidget->show();
    }

    if(currentComment == countComments-1){
        right->setEnabled(false);
        right->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-right: 170px;");
    }
    if(currentComment == 0){
        left->setEnabled(false);
        left->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-left: 170px;");
    }
}

void new_comments::setOkFalse()
{
    QSqlQuery query;//Создаем объект запроса
    query.prepare("UPDATE `comments` SET `ok` = :result, `otvet` = :otvet WHERE `id` = :id");
    query.bindValue(":id", comments[currentComment].id);
    query.bindValue(":result", 2);
    query.bindValue(":otvet", replyText->toPlainText());
    qDebug() << query.exec();


    if(loadComments() != 0)
    {
        if(currentComment > 0 && currentComment < countComments-1)
        {
            countComments = loadComments();
            showComment(currentComment);
        }
        if(currentComment == 0)
        {
            countComments = loadComments();
            showComment(currentComment);
        }
        else if(currentComment == countComments-1)
        {
            countComments = loadComments();
            currentComment = countComments-1;
            showComment(currentComment);
        }
        countComments = loadComments();
    } else
    {
        unMainWidget->hide();
        noCommentsWidget->show();
    }

    if(currentComment == countComments-1){
        right->setEnabled(false);
        right->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-right: 170px;");
    }
    if(currentComment == 0){
        left->setEnabled(false);
        left->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-left: 170px;");
    }

}

void new_comments::nextComment()
{
    if(currentComment+1 < countComments){
        currentComment++;
        showComment(currentComment);
    }
    if(countComments != 1)
    {
        left->setEnabled(true);
        left->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold; margin-left: 170px;");
    }
    if(currentComment+1 == countComments){
        right->setEnabled(false);
        right->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold; margin-right: 170px;");
    }
}

void new_comments::prewComment()
{
    if(currentComment > 0){
        currentComment--;
        showComment(currentComment);
    }
    if(countComments != 1)
    {
        right->setEnabled(true);
        right->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold; margin-right: 170px;");
    }
    if(currentComment == 0){
        left->setEnabled(false);
        left->setStyleSheet("padding: 15px; background-color: #787878; color: white; font-weight: bold;  margin-left: 170px;");
    }
}
