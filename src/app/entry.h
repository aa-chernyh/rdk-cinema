#ifndef ENTRY_H
#define ENTRY_H
#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "mainwindow.h"
#include <QSqlQuery>
#include <QCryptographicHash>


class entry : public QMainWindow
{
    Q_OBJECT
public:
    explicit entry(QWidget *parent = 0);
    QWidget *inputWidget;
    bool ok = false;
    MainWindow *w;
private:
    QVBoxLayout *inputLayer;
        QLineEdit *login;
        QLineEdit *password;
        QPushButton *logIn;

    QSqlDatabase db1;
private slots:
    void enter();
};

#endif // ENTRY_H
