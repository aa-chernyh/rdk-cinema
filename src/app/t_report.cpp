#include "t_report.h"

t_report::t_report()
{
    createTable();

    //Слои
    t_reportLayer = new QVBoxLayout;
    t_reportWidget = new QWidget;
        t_reportWidget->setWindowTitle("Отчет по бронированию");
        t_reportWidget->setWindowIcon(QIcon(":/Images/otchety.jpg"));
    t_reportLayer->addWidget(tableT_report);
    t_reportWidget->setLayout(t_reportLayer);
    t_reportWidget->setMinimumWidth(793);
    t_reportWidget->setMinimumHeight(700);
}

void t_report::createTable()
{
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT count(*) FROM current_films WHERE `nachalo` <= CURDATE()");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableT_report->clear();
    tableT_report->setColumnCount(6);
    tableT_report->setHorizontalHeaderLabels(QStringList() << "Название фильма" << "Бронировано билетов" << "На общую сумму, р" << "Из них оплачено" << "На общую сумму, р" << "Издержки, р");
    tableT_report->setRowCount(rowsCount);
    tableT_report->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableT_report->resizeColumnsToContents();
    tableT_report->setColumnWidth(0, 410);

    query.exec("SELECT `id`, `name` FROM `current_films` WHERE `nachalo` <= CURDATE()");//Запрашиваю ID фильмов и имена
    QSqlQuery query2;
    QSqlQuery query3;
    QSqlQuery query4;

    for(int i=0; query.next(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(1).toString()));
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        tableT_report->setItem(i, 0, item); //Записываю имена в таблицу

        query2.prepare("SELECT count(*) FROM `bron` WHERE `bron`.`film_id` = :id");//Запрашиваю количество забронированных билетов
        query2.bindValue(":id", query.value(0).toInt());
        query2.exec();
        if(query2.next())
        {
            item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toInt()));
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toInt()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 1, item);//И записываю
        }

        query4.prepare("SELECT count(*) FROM `bron` WHERE `bron`.`film_id` = :id AND `oplacheno` = 1");//Запрашиваю количество оплаченных билетов
        query4.bindValue(":id", query.value(0).toInt());
        query4.exec();
        if(query4.next())
        {
            item = new QTableWidgetItem(tr("%1").arg(query4.value(0).toInt()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 3, item);//И записываю
        }

        query3.prepare("SELECT `cena` FROM `sessions` WHERE `sessions`.`film_id` = :id");//Запрашиваю цену билетов
        query3.bindValue(":id", query.value(0).toInt());
        query3.exec();
        if(query3.next()){
            item = new QTableWidgetItem(tr("%1").arg(query3.value(0).toInt()*query2.value(0).toInt()));//Считаю на какую сумму забронировали билетов
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 2, item);

            item = new QTableWidgetItem(tr("%1").arg(query3.value(0).toInt()*query4.value(0).toInt()));//Считаю на какую сумму купили билетов
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 4, item);

            item = new QTableWidgetItem(tr("%1").arg(query3.value(0).toInt()*query2.value(0).toInt()-query3.value(0).toInt()*query4.value(0).toInt()));//Издержки
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 5, item);
        } else //Если сеансов на фильм не добавлено (не получилось достать цену билета)
        {
            item = new QTableWidgetItem(tr("%1").arg(0));// пишем 0
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 2, item);
            item = new QTableWidgetItem(tr("%1").arg(0));// пишем 0
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 4, item);
            item = new QTableWidgetItem(tr("%1").arg(0));// пишем 0
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            tableT_report->setItem(i, 5, item);
        }
    }
    tableT_report->resizeColumnsToContents();
    tableT_report->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableT_report->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableT_report->verticalHeader()->setDefaultSectionSize(45);
    tableT_report->horizontalHeader()->setFixedHeight(45);

    for(int i=1; i<6; i++)
        tableT_report->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
}
