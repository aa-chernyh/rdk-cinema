#ifndef F_REPORT_H
#define F_REPORT_H
#include <QDialog>
#include <QVBoxLayout>
#include <QSqlQuery>
#include <QTableWidget>
#include <QDebug>
#include <QHeaderView>


class f_report : public QDialog
{
    Q_OBJECT
public:
    f_report();
    QWidget *f_reportWidget;
private:
    QVBoxLayout *f_reportLayer;
    QTableWidget *tableF_report = new QTableWidget;
public slots:
    void createTable();
};

#endif // F_REPORT_H
