#ifndef SESSIONS_H
#define SESSIONS_H
#include <QDialog>
#include <QWidget>
#include <QGridLayout>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QStringList>
#include <QLabel>
#include <QSqlQuery>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include <QHeaderView>
#include "create_session.h"


class sessions : public QDialog
{
    Q_OBJECT

public:
    sessions();
    QWidget *sessionLayerWidget;

private:
    QGridLayout *sessionLayer;
        QTableWidget *tableSession = new QTableWidget;
        QVBoxLayout *tableSessionLayer;
        QHBoxLayout *tableButtons;

    QPushButton *removeSession;
    QPushButton *createSessionDialog;

    create_session *s = new create_session();

    bool tableReset = 0;

public slots:
    void createTable();
    void createTable(int sort);
    void removeRow();
    void changeElem(int row,int column);
    void callNewSession();

private slots:

};

#endif // SESSIONS_H
