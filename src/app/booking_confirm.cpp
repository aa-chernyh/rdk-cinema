#include "booking_confirm.h"

booking_confirm::booking_confirm()
{

}

booking_confirm::booking_confirm(int identif)
{
    identificator = identif;
    booking_confirmWidget = new QWidget;
        mainLayer = new QVBoxLayout;
            film = new QLabel;
            session = new QLabel;
            places = new QLabel;
            info = new QLabel(tr("<b>Информация о забронировавшем:</b>"));
            name = new QLabel;
                name->setStyleSheet("margin-left: 30px;");
            mail = new QLabel;
                mail->setStyleSheet("margin-left: 30px;");
            buttons = new QHBoxLayout;
                ok = new QPushButton(tr("Подтвердить оплату"));
                    ok->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                    connect(ok, SIGNAL(clicked()), SLOT(okBooking()));
                drop = new QPushButton(tr("Отменить бронь"));
                    drop->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                    connect(drop, SIGNAL(clicked(bool)), SLOT(removeBooking()));

    if(load(identificator))
    {
        buttons->addWidget(ok);
        buttons->addWidget(drop);
            mainLayer->addWidget(film);
            mainLayer->addWidget(session);
            mainLayer->addWidget(places);
            mainLayer->addWidget(info);
            mainLayer->addWidget(name);
            mainLayer->addWidget(mail);
            mainLayer->addLayout(buttons);

    } else{
        noResults = new QLabel("Нет результатов");
            noResults->setAlignment(Qt::AlignCenter);
            mainLayer->addWidget(noResults);
    }
    booking_confirmWidget->setLayout(mainLayer);
        booking_confirmWidget->setMinimumWidth(400);
        booking_confirmWidget->setMinimumHeight(200);
        booking_confirmWidget->setWindowTitle("Бронь №" + QString::number(identif));
        booking_confirmWidget->setWindowIcon(QIcon(":/Images/bron.jpg"));
}

bool booking_confirm::load(int id)
{
    QSqlQuery query;//Создаем объект запроса

    query.prepare("SELECT count(*) FROM `current_films`, `sessions`, `bron` WHERE `current_films`.`id` = `bron`.`film_id` AND `sessions`.`id` = `bron`.`ses_id` AND `bron`.`oplacheno` IS NULL AND `bron`.`identificator` = :id");
    query.bindValue(":id", id);
    query.exec();
    query.next();
    if(query.value(0).toInt() == 0)
        return false;

    query.prepare("SELECT `current_films`.`name`, `sessions`.`data`, `sessions`.`vremya`, `bron`.`place`, `bron`.`name`, `bron`.`mail` FROM `current_films`, `sessions`, `bron` WHERE `current_films`.`id` = `bron`.`film_id` AND `sessions`.`id` = `bron`.`ses_id` AND `bron`.`oplacheno` IS NULL AND `bron`.`identificator` = :id");
    query.bindValue(":id", id);
    query.exec();
    query.next();
    film->setText("Фильм: " + query.value(0).toString());
    session->setText("Сеанс: " + query.value(1).toString() + " в " + query.value(2).toString().section("", 0, 5));
    name->setText("Имя: " + query.value(4).toString());
    mail->setText("Почта: " + query.value(5).toString());

    QString str = "Места: <ul><li>Ряд " + query.value(3).toString().section("", 2, 3) +" место " + query.value(3).toString().section("", 5, 6) + "</li>";
    for(int i = 1; query.next(); i++)
    {
        str += "<li>Ряд " + query.value(3).toString().section("", 2, 3) +" место " + query.value(3).toString().section("", 5, 6) + "</li>";
    }
    str += "</ul>";
    places->setText(str);
    return true;
}

void booking_confirm::removeBooking()
{
    QMessageBox msg;
    msg.setWindowTitle("Отмена брони");
    msg.setText("Отменить?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        QSqlQuery query;//Создаем объект запроса
        query.prepare("DELETE FROM `kinonirb_rdk`.`bron` WHERE `bron`.`identificator` = :id");
        query.bindValue(":id", identificator);
        qDebug() << query.exec();
        booking_confirmWidget->close();
    }
}

void booking_confirm::okBooking()
{
    QMessageBox msg;
    msg.setWindowTitle("Подтверждение оплаты");
    msg.setText("Оплачено?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        QSqlQuery query;//Создаем объект запроса
        query.prepare("UPDATE `kinonirb_rdk`.`bron` SET `oplacheno` = '1' WHERE `bron`.`identificator` = :id");
        query.bindValue(":id", identificator);
        qDebug() << query.exec();
        booking_confirmWidget->close();
    }
}
