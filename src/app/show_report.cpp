#include "show_report.h"

show_report::show_report(QString querySQL, int cols)
{
    show_reportWidget = new QWidget;
        mainLayer = new QVBoxLayout;
            table = new QTableWidget;

    zaprosSQL = querySQL;
    countColumns = cols;
    createTable();
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    mainLayer->addWidget(table);
        show_reportWidget->setLayout(mainLayer);
        show_reportWidget->setMinimumWidth(cols*140+65);
        show_reportWidget->setMinimumHeight(700);
        show_reportWidget->setWindowTitle("Сборный отчет");
        show_reportWidget->setWindowIcon(QIcon(":/Images/otchety.jpg"));
        for(int i=0; i<cols; i++)
            table->horizontalHeader()->setSectionResizeMode(i, QHeaderView::Stretch);
        show_reportWidget->QWidget::showMaximized();

    //Реакция нажатия на столбцы (сортировка)
    connect(table->horizontalHeader(), SIGNAL(sectionClicked(int)), SLOT(createTable(int)));
}

void show_report::createTable()
{
    QSqlQuery query;//Создаем объект запроса
    query.exec(zaprosSQL);
    int rowsCount = 0;
    while(query.next())
        rowsCount++;

    //
    //Табличка
    //
    table->clear();
    table->setColumnCount(countColumns);
    table->setRowCount(rowsCount);

    query.exec(zaprosSQL);
    for(int i=0; query.next(); i++)
    {
        for(int j=0; j < countColumns; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            table->setItem(i, j, item);
        }
        table->setColumnWidth(i, 140);
    }

    QStringList translatedHorizontalHeaders;
    for(int i=0; i<countColumns; i++)
    {
        horizontalHeaders << query.record().fieldName(i);
        translatedHorizontalHeaders << translate(horizontalHeaders[i]);
    }

    table->setHorizontalHeaderLabels(translatedHorizontalHeaders);
    table->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    table->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    table->verticalHeader()->setDefaultSectionSize(45);
    table->horizontalHeader()->setFixedHeight(45);
    //table->verticalHeader()->hide();
}

void show_report::createTable(int sort)
{
    QSqlQuery query;//Создаем объект запроса

    QString tempSQL = zaprosSQL + " ORDER BY `" + horizontalHeaders[sort] + "`";
    query.exec(tempSQL);
    int rowsCount = 0;
    while(query.next())
        rowsCount++;

    //
    //Табличка
    //
    table->clear();
    table->setColumnCount(countColumns);
    table->setRowCount(rowsCount);

    query.exec(tempSQL);

    for(int i=0; query.next(); i++)
    {
        horizontalHeaders << query.record().fieldName(i);
        for(int j=0; j < countColumns; j++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(query.value(j).toString()));
            if(i%2 == 1)
                item->setBackgroundColor("#F0F0F0");
            item->setTextAlignment(Qt::AlignCenter);
            table->setItem(i, j, item);
        }
        table->setColumnWidth(i, 140);
    }

    QStringList translatedHorizontalHeaders;
    for(int i=0; i<countColumns; i++)
    {
        horizontalHeaders << query.record().fieldName(i);
        translatedHorizontalHeaders << translate(horizontalHeaders[i]);
    }

    table->setHorizontalHeaderLabels(translatedHorizontalHeaders);
    table->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    table->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    table->verticalHeader()->setDefaultSectionSize(45);
    table->horizontalHeader()->setFixedHeight(45);
    //table->verticalHeader()->hide();
}

QString show_report::translate(QString value)
{
    if(value == "film_name"){
        return "Название фильма";
    }
    if(value == "vertical_img"){
        return "Вертикальная картинка";
    }
    if(value == "horizontal_img"){
        return "Горизонтальная картинка";
    }
    if(value == "description"){
        return "Описание фильма";
    }
    if(value == "nachalo"){
        return "Начало проката";
    }
    if(value == "konec"){
        return "Окончание проката";
    }
    if(value == "zhanr"){
        return "Жанр фильма";
    }
    if(value == "vremya"){
        return "Время сеанса";
    }
    if(value == "data"){
        return "Дата сеанса";
    }
    if(value == "cena"){
        return "Цена";
    }
    if(value == "space"){
        return "Пространство";
    }
    if(value == "identificator"){
        return "Идентификатор брони";
    }
    if(value == "datavremya"){
        return "Дата и время бронирования";
    }
    if(value == "name"){
        return "Имя забронировавшего";
    }
    if(value == "mail"){
        return "Почта забронировавшего";
    }
    if(value == "place"){
        return "Место в зале";
    }
    if(value == "dney_prokata"){
        return "Дней проката";
    }
    return value;
}
