#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(int rules)
{
    setWindowTitle("Помощник по редактированию БД Кинотеатра");
    setWindowIcon(QIcon(":/Images/logo.png"));
    setMinimumWidth(1120);
    //setStyleSheet("background-color: #FFFFFF");


    //
    /*Верхнее меню программы*/
    //
    QMenu *file = menuBar()->addMenu("Файл");
        //close
        QAction *close = new QAction(tr("Выход"), this);
        close->setIcon(QIcon(":/Images/close.png"));
        close->setShortcut(QKeySequence::Close);
        file->addAction(close);
        connect(close, SIGNAL(triggered(bool)), this, SLOT(close()));

    QMenu *help = menuBar()->addMenu("Помощь");
        //Site
        QAction *site = new QAction(tr("На сайт"));
        help->addAction(site);
        site->setIcon(QIcon(":/Images/favicon.ico"));
        connect(site, SIGNAL(triggered(bool)), this, SLOT(goSite()));

        //About
        QAction *about = new QAction(tr("О программе"));
        help->addAction(about);
        about->setIcon(QIcon(":/Images/about.png"));
        connect(about, SIGNAL(triggered(bool)), this, SLOT(about()));

        //About author
        QAction *aboutAuthor = new QAction(tr("Об авторе"));
        help->addAction(aboutAuthor);
        aboutAuthor->setIcon(QIcon(":/Images/aboutAuthor.png"));
        connect(aboutAuthor, SIGNAL(triggered(bool)), this, SLOT(aboutAuthor()));

    //
    /**/
    //


    //
    /* Меню программы */
    //
    films_button = new QToolButton;
        films_button->setStyleSheet("background: url(:/Images/films.png)");
        films_button->setFixedSize(74,74);
        connect(films_button, SIGNAL(clicked()), SLOT(setFilmsLayer()));
    sessions_button = new QPushButton();
        sessions_button->setStyleSheet("background: url(:/Images/sessions.png)");
        sessions_button->setFixedSize(74,74);
        connect(sessions_button, SIGNAL(clicked()), SLOT(setSessionsLayer()));
    comments_button = new QPushButton();
        comments_button->setStyleSheet("background: url(:/Images/comments.png)");
        comments_button->setFixedSize(74,74);
        connect(comments_button, SIGNAL(clicked()), SLOT(setCommentsLayer()));
    new_comments_button = new QPushButton();
        new_comments_button->setStyleSheet("background: url(:/Images/n_comments.png)");
        new_comments_button->setFixedSize(74,74);
        connect(new_comments_button, SIGNAL(clicked(bool)), SLOT(setNewCommentsLayer()));
    booking_button = new QPushButton();
        booking_button->setStyleSheet("background: url(:/Images/booking.png)");
        booking_button->setFixedSize(74,74);
        connect(booking_button, SIGNAL(clicked(bool)), SLOT(setBookingLayer()));
    create_report_button = new QPushButton();
        create_report_button->setStyleSheet("background: url(:/Images/reports.png)");
        create_report_button->setFixedSize(74,74);
        connect(create_report_button, SIGNAL(clicked(bool)), SLOT(setReportsLayer()));

    menu = new QVBoxLayout;
    if(rules == 0)
    {
        menu->addWidget(films_button);
        menu->addWidget(sessions_button);
        menu->addWidget(comments_button);
        menu->addWidget(new_comments_button);
        menu->addWidget(booking_button);
        menu->addWidget(create_report_button);
    }
    if(rules == 1)
    {
        menu->addWidget(films_button);
        menu->addWidget(sessions_button);
        menu->addWidget(booking_button);
    }
        menu->setMargin(0);
    menuWidget = new QWidget;
        menuWidget->setLayout(menu);
        menuWidget->setFixedWidth(74);
        menuWidget->setFixedHeight(450);
        menuWidget->setGeometry(40, 40, 74, 450);
    if(rules == 1){
        menuWidget->setFixedHeight(240);
        setMinimumHeight(500);
    }
    //
    /**/
    //


    films_object = new films;
    sessions_object = new sessions;
    comments_object = new comments;
    newComments_object = new new_comments;
    booking_object = new booking;
    reports_object = new reports;

    mainLayer = new QHBoxLayout;
        mainLayer->addWidget(menuWidget);
        mainLayer->addWidget(films_object->filmLayerWidget);
        mainLayer->addWidget(sessions_object->sessionLayerWidget);
            sessions_object->sessionLayerWidget->hide();
        mainLayer->addWidget(comments_object->commentsLayerWidget);
            comments_object->commentsLayerWidget->hide();
        mainLayer->addWidget(newComments_object->newCommentsWidget);
            newComments_object->newCommentsWidget->hide();
        mainLayer->addWidget(booking_object->bookingWidget);
            booking_object->bookingWidget->hide();
        mainLayer->addWidget(reports_object->reportsWidget);
            reports_object->reportsWidget->hide();
    mainWidget = new QWidget;
        mainWidget->setLayout(mainLayer);
    setCentralWidget(mainWidget);

    //QWidget::showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
    db.close();
}

void MainWindow::about()
{
    QMessageBox msgAbout;
    msgAbout.setText("<p align='justify'>Программа создана специально для кинотеатра Никифоровского дома культуры в целях выпускной квалификационной работы бакалавром Тамбовского Технического Государственного Университета.</p>");
    msgAbout.setWindowTitle("О программе");
    msgAbout.setIcon(QMessageBox::Information);
    msgAbout.setWindowIcon(QIcon(":/Images/info.jpg"));
    msgAbout.exec();
}

void MainWindow::aboutAuthor()
{
    QMessageBox msgAboutAuthor;
    msgAboutAuthor.setText("<h4 align='center'>Создатель Александр Черных</h4> <p align='justify'>Связаться с автором можно с помощью:</p> <ul><li>ВКонтакте: https://vk.com/id146590181;</li> <li>почты: AA-Chernyh@mail.ru.</li></ul>");
    msgAboutAuthor.setWindowTitle("Об авторе");
    msgAboutAuthor.setIcon(QMessageBox::Information);
    msgAboutAuthor.setWindowIcon(QIcon(":/Images/info.jpg"));
    msgAboutAuthor.exec();
}

void MainWindow::goSite()
{
    QDesktopServices::openUrl(QUrl("http://кино-рдк.рф"));
}

void MainWindow::hideWidgets()
{
    films_object->filmLayerWidget->hide();
    sessions_object->sessionLayerWidget->hide();
    comments_object->commentsLayerWidget->hide();
    newComments_object->newCommentsWidget->hide();
    booking_object->bookingWidget->hide();
    reports_object->reportsWidget->hide();
}

void MainWindow::setFilmsLayer()
{
    hideWidgets();
    films_object->filmLayerWidget->show();
}

void MainWindow::setSessionsLayer()
{
    hideWidgets();
    sessions_object->sessionLayerWidget->show();
}

void MainWindow::setCommentsLayer()
{
    hideWidgets();
    comments_object->commentsLayerWidget->show();
}

void MainWindow::setNewCommentsLayer()
{
    hideWidgets();
    newComments_object->newCommentsWidget->show();
}

void MainWindow::setBookingLayer()
{
    hideWidgets();
    booking_object->bookingWidget->show();
}

void MainWindow::setReportsLayer()
{
    hideWidgets();
    reports_object->reportsWidget->show();
}
