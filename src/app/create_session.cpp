#include "create_session.h"

create_session::create_session()
{
    //setStyleSheet("background-color: #FFFFFF");
    //
    //workAreaSession
    //

    //Название
    QLabel *nameFilm = new QLabel("Название: ");
        nameFilm->setStyleSheet("font-weight: bold");
    nameFilmEdit = new QComboBox;
        setFilmNames();
        nameFilmEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F8F8F8;");

    //Дата
    QLabel *data = new QLabel("Дата: ");
        data->setStyleSheet("font-weight: bold");
    dataEdit = new QLineEdit;
        dataEdit->setPlaceholderText(tr("ГГГГ-ММ-ДД"));
        dataEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Время
    QLabel *vremya = new QLabel("Время: ");
        vremya->setStyleSheet("font-weight: bold");
    hh = new QComboBox;
        QStringList hoursList;
        hoursList << "00"
                  << "01"
                  << "02"
                  << "03"
                  << "04"
                  << "05"
                  << "06"
                  << "07"
                  << "08"
                  << "09"
                  << "10"
                  << "11"
                  << "12"
                  << "13"
                  << "14"
                  << "15"
                  << "16"
                  << "17"
                  << "18"
                  << "19"
                  << "20"
                  << "21"
                  << "22"
                  << "23";
        hh->addItems(hoursList);
        hh->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F8F8F8;");
        hh->setMinimumWidth(90);
    mm = new QComboBox;
        QStringList minutesList;
        minutesList << "00"
                    << "05"
                    << "10"
                    << "15"
                    << "20"
                    << "25"
                    << "30"
                    << "35"
                    << "40"
                    << "45"
                    << "50"
                    << "55";
        mm->addItems(minutesList);
        mm->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F8F8F8;");
        mm->setMinimumWidth(90);
    dvoetochie = new QLabel(":");
    timeLayer = new QHBoxLayout;
        timeLayer->addWidget(hh);
        timeLayer->addWidget(dvoetochie);
        timeLayer->addWidget(mm);

    //Цена
    QLabel *cena = new QLabel("Цена: ");
        cena->setStyleSheet("font-weight: bold");
    cenaEdit = new QLineEdit;
        cenaEdit->setPlaceholderText(tr("Только цифры"));
        cenaEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");

    //Пространство
    QLabel *space = new QLabel("Пространство: ");
        space->setStyleSheet("font-weight: bold");
    spaceEdit = new QComboBox;
    QStringList spaces;
    spaces << "2D" << "3D";
        spaceEdit->addItems(spaces);
        spaceEdit->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F8F8F8;");
        spaceEdit->setMinimumWidth(190);

    //Галочка на несколько дней
    galochkaLayer = new QHBoxLayout;
        galochka = new QCheckBox;
        textLeft = new QLabel("То же расписание на ");
        countDays = new QLineEdit;
            countDays->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0; margin-top: 10px; margin-bottom: 10px;");
        textRight = new QLabel(" д. вперед включительно");
    galochkaLayer->addWidget(galochka);
    galochkaLayer->addWidget(textLeft);
    galochkaLayer->addWidget(countDays);
    galochkaLayer->addWidget(textRight);

    //Кнопки
    addSession = new QPushButton(tr("Добавить сеанс(ы)"));
        addSession->setDefault(true);
        addSession->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(addSession, SIGNAL(clicked(bool)), SLOT(createSession()));
        addSession->setShortcut(Qt::Key_Return);
    resetSession = new QPushButton(tr("Сбросить"));
        resetSession->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(resetSession, SIGNAL(clicked(bool)), SLOT(resetInputs()));

    //
    //Слои
    //
    sessionLayer = new QVBoxLayout;
    workAreaSession = new QGridLayout;
        workAreaSession->addWidget(nameFilm, 0,0, 1,2);
        workAreaSession->addWidget(nameFilmEdit, 1,0, 1,2);
        workAreaSession->addWidget(data, 2,0);
        workAreaSession->addWidget(dataEdit, 3,0);
        workAreaSession->addWidget(vremya, 2,1);
        workAreaSession->addLayout(timeLayer, 3,1);
        workAreaSession->addWidget(cena, 4,0);
        workAreaSession->addWidget(cenaEdit, 5,0);
        workAreaSession->addWidget(space, 4,1);
        workAreaSession->addWidget(spaceEdit, 5,1);

    workAreaSessionButtons = new QHBoxLayout;
        workAreaSessionButtons->addWidget(addSession);
        workAreaSessionButtons->addWidget(resetSession);

    sessionLayer->addLayout(workAreaSession);
    sessionLayer->addLayout(galochkaLayer);
    sessionLayer->addLayout(workAreaSessionButtons);

    sessionLayerWidget = new QWidget;
    sessionLayerWidget->setLayout(sessionLayer);
        sessionLayerWidget->setWindowTitle(tr("Создание сеанса"));
        sessionLayerWidget->setMinimumWidth(400);
        sessionLayerWidget->setWindowIcon(QIcon(":/Images/seansy.jpg"));

}

void create_session::createSession()
{
    QSqlQuery query;
    query.prepare("SELECT `id` FROM `current_films` WHERE `name` = :film_name");
    query.bindValue(":film_name", nameFilmEdit->currentText());
    query.exec();
    query.next();
    int film_id = query.value(0).toInt();

    if(galochka->isChecked())
    {
        for(int i=0; i<=countDays->text().toInt(); i++)
        {
            query.prepare("INSERT INTO `kinonirb_rdk`.`sessions` VALUES (NULL, :vremya, ADDDATE(:data, :count), :cena, :space, :film_id)");
            query.bindValue(":vremya", hh->currentText() + ":" + mm->currentText());
            query.bindValue(":data", dataEdit->text());
            query.bindValue(":cena", cenaEdit->text());
            query.bindValue(":space", spaceEdit->currentText());
            query.bindValue(":film_id", film_id);
            query.bindValue(":count", i);
            qDebug() << query.exec();
        }
    }
    else
    {
        query.prepare("INSERT INTO `kinonirb_rdk`.`sessions` VALUES (NULL, :vremya, :data, :cena, :space, :film_id)");
        query.bindValue(":vremya", hh->currentText() + ":" + mm->currentText());
        query.bindValue(":data", dataEdit->text());
        query.bindValue(":cena", cenaEdit->text());
        query.bindValue(":space", spaceEdit->currentText());
        query.bindValue(":film_id", film_id);
        qDebug() << query.exec();
    }
    resetInputs();
    sessionLayerWidget->close();
}

void create_session::setFilmNames()
{
    nameFilmEdit->clear();
    QStringList filmy;
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT `name` FROM current_films");
    while(query.next())
    {
        filmy.append(query.value(0).toString());
    }
    nameFilmEdit->addItems(filmy);
}

void create_session::resetInputs()
{
    nameFilmEdit->setCurrentIndex(0);
    hh->setCurrentIndex(0);
    mm->setCurrentIndex(0);
    dataEdit->clear();
    cenaEdit->clear();
    spaceEdit->setCurrentIndex(0);
    setFilmNames();
    galochka->setChecked(false);
}
