#ifndef BOOKING_CONFIRM_H
#define BOOKING_CONFIRM_H
#include <QDialog>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSqlQuery>
#include <QString>
#include <QDebug>
#include <QMessageBox>

class booking_confirm : public QDialog
{
    Q_OBJECT
public:
    booking_confirm();
    booking_confirm(int identif);
    QWidget *booking_confirmWidget;

    QPushButton *ok;
    QPushButton *drop;
private:
    QVBoxLayout *mainLayer;
        QLabel *film;
        QLabel *session;
        QLabel *places;
        QLabel *info;
        QLabel *name;
        QLabel *mail;
        QHBoxLayout *buttons;


    QLabel *noResults;
    int identificator = 0; //временный идентификатор

public slots:
    bool load(int id);
    void removeBooking();
    void okBooking();
};

#endif // BOOKING_CONFIRM_H
