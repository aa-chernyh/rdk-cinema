#include "booking.h"

booking::booking()
{
    bookingWidget = new QWidget;
        //bookingWidget->setStyleSheet("background-color: #FFFFFF");
        mainLayer = new QVBoxLayout;
            layerID = new QVBoxLayout;
                inputID = new QLineEdit;
                    inputID->setPlaceholderText(tr("Идентификатор брони"));
                    inputID->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; background-color: #F0F0F0;");
                buttonID = new QPushButton(tr("Найти"));
                    buttonID->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
                    connect(buttonID, SIGNAL(clicked(bool)), SLOT(searchBook()));
            layerMail = new QVBoxLayout;
                inputMail = new QLineEdit;
                    inputMail->setPlaceholderText(tr("или Почта"));
                    inputMail->setStyleSheet("border: none; border-bottom: 1px  solid #191919; padding: 5px 0; margin-right: 150px;");
                buttonMail = new QPushButton(tr("Найти"));
                    buttonMail->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold; margin-right: 150px;");

    removeBron = new QPushButton(tr("Удалить бронь (строку)"));
        removeBron->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(removeBron, SIGNAL(clicked()), SLOT(removeRow()));
    payBron = new QPushButton(tr("Подтвердить оплату"));
        payBron->setStyleSheet("padding: 15px; background-color: #191919; color: white; font-weight: bold;");
        connect(payBron, SIGNAL(clicked(bool)), SLOT(okBook()));

    tableBronLayer = new QVBoxLayout;
        tableBronLayer->addWidget(tableBronEdit);
        tableButtons = new QHBoxLayout;
            tableButtons->addWidget(removeBron);
            tableButtons->addWidget(payBron);
        tableBronLayer->addLayout(tableButtons);
    createTable();

    layerID->addWidget(inputID);
    layerID->addWidget(buttonID);
    //layerMail->addWidget(inputMail);
    //layerMail->addWidget(buttonMail);
        mainLayer->addLayout(tableBronLayer);
        mainLayer->addLayout(layerID);
        //mainLayer->addLayout(layerMail);
            bookingWidget->setLayout(mainLayer);
            //bookingWidget->setFixedHeight(150);

    //Реакция нажатия на столбцы (сортировка)
    connect(tableBronEdit->horizontalHeader(), SIGNAL(sectionClicked(int)), SLOT(createTable(int)));

}

void booking::searchBook()
{
    bk = new booking_confirm(inputID->text().toInt());
    bk->booking_confirmWidget->show();
    connect(bk->ok, SIGNAL(clicked(bool)), this, SLOT(createTable()));
    connect(bk->drop, SIGNAL(clicked(bool)), this, SLOT(createTable()));
}

void booking::createTable()
{
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT COUNT(*) FROM `bron` WHERE `oplacheno` is NULL");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableBronEdit->clear();
    tableBronEdit->setColumnCount(7);
    tableBronEdit->setRowCount(rowsCount);
    tableBronEdit->setHorizontalHeaderLabels(QStringList() << "Название фильма" << "Номер брони" << "Дата" << "Время" << "Место" << "Имя" << "Почта");

    query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL");

    QStringList VerticalHeaders;
    QTableWidgetItem *item;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();

        QSqlQuery query2;
        query2.prepare("SELECT `name` FROM `current_films` WHERE `id` = :id");
        query2.bindValue(":id", query.value(8).toInt());
        query2.exec();
        query2.next();
        item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toString()));//Название фильма
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 0, item);

        QSqlQuery query3;
        query3.prepare("SELECT `data`, `vremya` FROM `sessions` WHERE `id` = :id");
        query3.bindValue(":id", query.value(3).toInt());
        query3.exec();
        query3.next();
        item = new QTableWidgetItem(tr("%1").arg(query.value(1).toString()));//Номер брони
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 1, item);
        item = new QTableWidgetItem(tr("%1").arg(query3.value(0).toString()));//Дата
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 2, item);
        item = new QTableWidgetItem(tr("%1").arg(query3.value(1).toString()));//Время
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 3, item);

        item = new QTableWidgetItem(tr("%1").arg(query.value(6).toString()));//Место
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 4, item);
        item = new QTableWidgetItem(tr("%1").arg(query.value(4).toString()));//Имя
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 5, item);
        item = new QTableWidgetItem(tr("%1").arg(query.value(5).toString()));//Почта
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 6, item);
    }
    tableBronEdit->setVerticalHeaderLabels(VerticalHeaders);
    tableBronEdit->resizeColumnsToContents();
    tableBronEdit->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableBronEdit->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableBronEdit->verticalHeader()->setDefaultSectionSize(45);
    tableBronEdit->horizontalHeader()->setFixedHeight(45);
    tableBronEdit->verticalHeader()->hide();
}

void booking::createTable(int sort)
{
    QSqlQuery query;//Создаем объект запроса
    query.exec("SELECT COUNT(*) FROM `bron` WHERE `oplacheno` is NULL");
    query.next();
    int rowsCount = query.value(0).toInt();

    //
    //Табличка
    //
    tableBronEdit->clear();
    tableBronEdit->setColumnCount(7);
    tableBronEdit->setRowCount(rowsCount);
    tableBronEdit->setHorizontalHeaderLabels(QStringList() << "Название фильма" << "Номер брони" << "Дата" << "Время" << "Место" << "Имя" << "Почта");

    switch(sort){
    case 1: query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL ORDER BY `identificator`"); break;
    case 4: query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL ORDER BY `place`"); break;
    case 5: query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL ORDER BY `name`"); break;
    case 6: query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL ORDER BY `mail`"); break;
    default: query.exec("SELECT * FROM `bron` WHERE `oplacheno` is NULL ORDER BY `identificator`");
    }

    QStringList VerticalHeaders;
    QTableWidgetItem *item;
    for(int i=0; query.next(); i++)
    {
        VerticalHeaders << query.value(0).toString();

        QSqlQuery query2;
        query2.prepare("SELECT `name` FROM `current_films` WHERE `id` = :id");
        query2.bindValue(":id", query.value(8).toInt());
        query2.exec();
        query2.next();
        item = new QTableWidgetItem(tr("%1").arg(query2.value(0).toString()));//Название фильма
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 0, item);

        QSqlQuery query3;
        query3.prepare("SELECT `data`, `vremya` FROM `sessions` WHERE `id` = :id");
        query3.bindValue(":id", query.value(3).toInt());
        query3.exec();
        query3.next();
        item = new QTableWidgetItem(tr("%1").arg(query.value(1).toString()));//Номер брони
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 1, item);
        item = new QTableWidgetItem(tr("%1").arg(query3.value(0).toString()));//Дата
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 2, item);
        item = new QTableWidgetItem(tr("%1").arg(query3.value(1).toString()));//Время
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 3, item);

        item = new QTableWidgetItem(tr("%1").arg(query.value(6).toString()));//Место
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 4, item);
        item = new QTableWidgetItem(tr("%1").arg(query.value(4).toString()));//Имя
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 5, item);
        item = new QTableWidgetItem(tr("%1").arg(query.value(5).toString()));//Почта
        if(i%2 == 1)
            item->setBackgroundColor("#F0F0F0");
        item->setTextAlignment(Qt::AlignCenter);
        tableBronEdit->setItem(i, 6, item);
    }
    tableBronEdit->setVerticalHeaderLabels(VerticalHeaders);
    tableBronEdit->resizeColumnsToContents();
    tableBronEdit->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    tableBronEdit->horizontalHeader()->setStyleSheet("color: white; font-weight: bold");
    tableBronEdit->setStyleSheet("QHeaderView::section::horizontal {background-color: #191919}");
    tableBronEdit->verticalHeader()->setDefaultSectionSize(45);
    tableBronEdit->horizontalHeader()->setFixedHeight(45);
    tableBronEdit->verticalHeader()->hide();
}


void booking::removeRow()
{
    int bron_id = tableBronEdit->verticalHeaderItem(tableBronEdit->currentRow())->text().toInt();

    QMessageBox msg;
    msg.setWindowTitle(tableBronEdit->item(tableBronEdit->currentRow(),0)->text());
    msg.setText("Удалить бронь?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        tableBronEdit->removeRow(tableBronEdit->currentRow()); //удалили строчку
        QSqlQuery query;
        query.prepare("DELETE FROM `kinonirb_rdk`.`bron` WHERE `bron`.`id` = :id;");
        query.bindValue(":id", bron_id);
        qDebug() << query.exec();
    }
}

void booking::okBook()
{
    QMessageBox msg;
    msg.setWindowTitle("Оплата");
    msg.setText("Подтвердить оплату (идентификатор " + tableBronEdit->item(tableBronEdit->currentRow(), 1)->text() + ")?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msg.setDefaultButton(QMessageBox::Yes);
    msg.setIcon(QMessageBox::Question);
    msg.setWindowIcon(QIcon(":/Images/question.png"));
    int result = msg.exec();
    if(result == QMessageBox::Yes)
    {
        QSqlQuery query;
        query.prepare("UPDATE `kinonirb_rdk`.`bron` SET `oplacheno`= 1 WHERE `id`=:id");
        query.bindValue(":id", tableBronEdit->verticalHeaderItem(tableBronEdit->currentRow())->text().toInt());
        qDebug() << query.exec();
        createTable();
    }
}
